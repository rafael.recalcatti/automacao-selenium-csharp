﻿using InclusaoProposta.tests.utils;
using System;
using System.Data.SqlClient;


namespace InclusaoProposta.tests.db
{
    class Conn
    {
        static public SqlConnection conn = new SqlConnection();

        public SqlConnection ReturnConnection(string connetionString)
        {

            try
            {
                conn.ConnectionString = connetionString;
                conn.Open();
                //conn.Close();
                return conn;

            }

            catch (Exception ex)
            {
                SetUp.GetInstance().MessageError.Add( " ERRO NA CONEXAO COM A BASE: "+ex.ToString());
                return null;
            }
        }
      }

    }
