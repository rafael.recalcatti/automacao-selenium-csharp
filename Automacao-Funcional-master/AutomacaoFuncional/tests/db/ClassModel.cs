﻿using InclusaoProposta.tests.utils;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace InclusaoProposta.tests.db
{
    class ClassModel
    {

        private Conn conn = new Conn();
        private static ClassModel classModel;
        private string baseExecucao;
        private SqlConnection connection = null;
        private SqlCommand command = null;
        private string serverBanco;
        private string baseBancoResultado;
        private string baseBancoExecucao;
        private string stringConnectResult;
        private string stringConnectExecution;
        private string strAmbiente;

        private ClassModel()
        {
            conn = new Conn();
            baseExecucao = SetUp.GetInstance().BaseExecucao;
            serverBanco = (baseExecucao.Equals("Teste") || baseExecucao.Equals("Desenvolvimento") ? "SQLTESDES" : "SQLESPHOM");
            baseBancoResultado = baseExecucao.Equals("Espelho") ? "AUTOMACAOesp" :
                                             baseExecucao.Equals("Homologação") ? "AUTOMACAOhom" :
                                             baseExecucao.Equals("Teste") ? "AUTOMACAOtest" : "AUTOMACAOdsv";

            baseBancoExecucao = baseExecucao.Equals("Espelho") ? "DFOLHAesp" :
                                             baseExecucao.Equals("Homologação") ? "DFOLHAhom" :
                                             baseExecucao.Equals("Teste") ? "DFOLHAtest" : "DFOLHAdsv";

            stringConnectResult = "Data Source = " + serverBanco + ", 1433; Initial Catalog = " + baseBancoResultado + "; User ID = web_users; Password = guest";

            stringConnectExecution = "Data Source = " + serverBanco + ", 1433; Initial Catalog = " + baseBancoExecucao + "; User ID = web_users; Password = guest";

            strAmbiente = baseExecucao.Equals("Espelho") ? "ESP" :
                                             baseExecucao.Equals("Homologação") ? "HOM" :
                                             baseExecucao.Equals("Teste") ? "TEST" : "DES";
        }

        public static ClassModel GetInstance()
        {
            if (classModel == null)
            {
                classModel = new ClassModel();
            }

            return classModel;
        }

        public bool ExecuteUpDateModalOportunidades(string status)
        {

            try
            {
                ReadFiles readFiles = new ReadFiles();
                connection = conn.ReturnConnection(stringConnectExecution);

                String query = @"UPDATE AFPAR_PARAMETRO     
                                SET VLR_PARAMETRO = @paraMetro
                                WHERE COD_PARAMETRO = 864 OR  NME_PARAMETRO LIKE '%ATIVA_TELA_OPORTUNIDADES%'";


                command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("@paraMetro", status));
                int ret = command.ExecuteNonQuery();
                command.Dispose();

                return ret > 0;

            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                try
                {
                    connection.Close();
                }
                catch (Exception)
                {

                }
            }
        }

        public List<string> BuscaCenarioSemMassa()
        {
            try
            {

                command = new SqlCommand();
                connection = conn.ReturnConnection(stringConnectResult);

                string query = "SELECT cha.ID_CENARIO FROM selenium.ATCHI_CENARIOS_HABILITADOS_INCLUSAO cha LEFT JOIN selenium.ATCXC_CENARIO_X_CPF ccp ON cha.ID_CENARIO = ccp.ID_CENARIO WHERE ccp.ID_CENARIO IS NULL;";
                List<string> list = new List<string>();
                command = new SqlCommand(query, connection);
                command.CommandTimeout = 600;
                SqlDataReader reader = command.ExecuteReader();

                {
                    while (reader.Read())
                    {
                        list.Add(reader["ID_CENARIO"].ToString());
                    }
                    return list;
                }


            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                try
                {
                    connection.Close();
                }
                catch (Exception)
                {

                }
            }
        }

        public bool ExecutaCursor(string arg)
        {

            try
            {
                ReadFiles readFiles = new ReadFiles();
                string ambiente = arg.Equals("Espelho") ? "ESP" : arg.Equals("Homologação") ? "HOM" : arg.Equals("Teste") ? "TEST" : "DES";
                command = new SqlCommand();

                connection = conn.ReturnConnection(stringConnectExecution);

                String query = readFiles.ReadFile(@"\tests\files\querys\" + ambiente + "_CURSOR_DESTIVA_CENARIOS.sql");

                command = new SqlCommand(query, connection);
                command.CommandTimeout = 3600;
                int ret = command.ExecuteNonQuery();

                query = readFiles.ReadFile(@"\tests\files\querys\" + ambiente + "_CURSOR_MASSAS_ANT.sql");

                command = new SqlCommand(query, connection);
                command.CommandTimeout = 3600;
                ret = command.ExecuteNonQuery();

                query = readFiles.ReadFile(@"\tests\files\querys\" + ambiente + "_CURSOR_MASSAS_REFIN.sql");
                command = new SqlCommand(query, connection);
                command.CommandTimeout = 3600;
                ret = command.ExecuteNonQuery();

                query = readFiles.ReadFile(@"\tests\files\querys\" + ambiente + "_CURSOR_MASSAS_NOVO.sql");
                command = new SqlCommand(query, connection);
                command.CommandTimeout = 3600;
                ret = command.ExecuteNonQuery();


                command.Dispose();

                return ret > 0;

            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                try
                {
                    connection.Close();
                }
                catch (Exception)
                {

                }

            }
        }

        internal bool LimparPropostaCPF(List<string> list_cpf)
        {
            try
            {
                string cpfCliente = "";

                for (int i = 0; i < list_cpf.Count; i++)
                {
                    if (i != 0)
                    {
                        cpfCliente += ",";
                    }

                    cpfCliente += "'" + list_cpf[i].Split(';')[0] + "'";
                }

                connection = conn.ReturnConnection("Data Source = SQLESPHOM,1433; Initial Catalog = DFOLHAesp; User ID = web_users; Password = guest");

                command = new SqlCommand();

                string query = "UPDATE AFEPP_EMPRESTIMO_PROPOSTA SET COD_SITUACAO_AF = 1"
                    + " WHERE COD_PROPOSTA IN(SELECT EPP.COD_PROPOSTA FROM AFEPP_EMPRESTIMO_PROPOSTA EPP"
                    + " INNER JOIN AFPRF_PROPOSTA_FUNCIONARIO PRF"
                    + " ON PRF.COD_PROPOSTA = EPP.COD_PROPOSTA"
                    + " INNER JOIN AFPFI_PESSOA_FISICA PFI"
                    + " ON PFI.COD_PESSOA_FISICA = PRF.COD_PESSOA_FISICA"
                    + " INNER JOIN AFCST_COD_SITUACAO_AF CST"
                    + " ON CST.COD_SITUACAO_AF = EPP.COD_SITUACAO_AF"
                    + " WHERE PFI.CPF IN(" + cpfCliente + ") AND COD_SITUACAO_AF IN (2,3))";


                command = new SqlCommand(query, connection);
                command.CommandTimeout = 600;


                //command.Parameters.Add(new SqlParameter("@cpfCliente", (cpfCliente == null ? "" : cpfCliente)));


                int res = command.ExecuteNonQuery();
                command.Dispose();
                return res > 0;


            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                try
                {
                    connection.Close();
                }
                catch (Exception)
                {

                }
            }
        }

        public int AbreResultadoTesteBase(
          string codigoCenario,
          string ambienteExecucao,
          string baseExecucao,
          string orgao,
          string financeira,
          string tipoCliente,
          string tipoOperacao,
          string codigoRepresentante,
          string loja)
        {

            try
            {

                connection = conn.ReturnConnection(stringConnectResult);

                command = new SqlCommand();

                string query = "INSERT INTO [selenium].[ATIPR_INCLUSAO_PROPOSTA]" +
                    " ([DTA_INICIO]," +
                    "[ID_CENARIO]," +
                    "[NME_AMBIENTE_EXEC_REG]," +
                    "[NME_AMBIENTE_EXT_MASSA]," +
                    "[NME_ORGAO]," +
                    "[NME_FINANCEIRA]," +
                    "[TPO_CLIENTE]," +
                    "[TPO_OPERACAO]," +
                    "[COD_REPR]," +
                    "[STA_LOJA]," +
                    "[NME_MAQUINA])" +
                    " VALUES" +
                    "(GETDATE()," +
                    "@idCenario," +
                    "@ambienteExe," +
                    "@baseExe," +
                    "@orgao," +
                    "@financeira," +
                    "@tipoCliente," +
                    "@tipoOperacao," +
                    "@codRepresentante," +
                    "@statusLoja," +
                    "@nomeMaquina) SELECT SCOPE_IDENTITY();";


                command = new SqlCommand(query, connection);

                string nomeMaquina = Environment.MachineName;

                command.Parameters.Add(new SqlParameter("@idCenario", codigoCenario));
                command.Parameters.Add(new SqlParameter("@ambienteExe", ambienteExecucao));
                command.Parameters.Add(new SqlParameter("@baseExe", baseExecucao));
                command.Parameters.Add(new SqlParameter("@orgao", orgao));
                command.Parameters.Add(new SqlParameter("@financeira", financeira));
                command.Parameters.Add(new SqlParameter("@tipoCliente", tipoCliente));
                command.Parameters.Add(new SqlParameter("@tipoOperacao", tipoOperacao));
                command.Parameters.Add(new SqlParameter("@codRepresentante", codigoRepresentante));
                command.Parameters.Add(new SqlParameter("@statusLoja", loja));
                command.Parameters.Add(new SqlParameter("@nomeMaquina", nomeMaquina));

                int primaryKey = Convert.ToInt32(command.ExecuteScalar());
                command.Dispose();
                return primaryKey;

            }
            catch (Exception e)
            {
                return 0;
            }
            finally
            {
                try
                {
                    connection.Close();
                }
                catch (Exception)
                {

                }
            }
        }

        public bool FechaResultadoTesteBase(
            int idTest,
            string codigoProposta,
            string codigoPropostaAprovada,
            string codigoTabela,
            string resultadoProposta,
            string cpfCliente)
        {
            try
            {

                connection = conn.ReturnConnection(stringConnectResult);

                command = new SqlCommand();

                string query = "UPDATE [selenium].[ATIPR_INCLUSAO_PROPOSTA] SET" +
                    " [DTA_FIM] = GETDATE()," +
                    " [COD_PROPOSTA] = @codProposta," +
                    " [COD_PROPOSTA_AP] = @codPropostaAprovada," +
                    " [COD_TABELA] = @codTabela," +
                    " [DSC_RESULTADO] = @resultado," +
                    " [CPF_CLIENTE] = @cpfCliente" +
                    " WHERE [ID_INCLUSAO] = @idInclusao;";

                command = new SqlCommand(query, connection);

                string _machineName = Environment.MachineName;
                command.Parameters.Add(new SqlParameter("@idInclusao", idTest));
                command.Parameters.Add(new SqlParameter("@codProposta", (codigoProposta == null ? "0" : codigoProposta)));
                command.Parameters.Add(new SqlParameter("@codPropostaAprovada", (codigoProposta == null ? "0" : codigoPropostaAprovada.All(char.IsDigit) ? codigoPropostaAprovada : "")));
                command.Parameters.Add(new SqlParameter("@codTabela", (codigoTabela == null ? "" : codigoTabela)));
                command.Parameters.Add(new SqlParameter("@resultado", (resultadoProposta == null ? "" : resultadoProposta)));
                command.Parameters.Add(new SqlParameter("@cpfCliente", (cpfCliente == null ? "" : cpfCliente)));

                int res = command.ExecuteNonQuery();
                command.Dispose();
                return res > 0;

            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                try
                {
                    connection.Close();
                }
                catch (Exception)
                {

                }
            }
        }

        public int AbreResultadoPassoBase(int idTeste)
        {
            try
            {

                connection = conn.ReturnConnection(stringConnectResult);

                command = new SqlCommand();

                string query = @"INSERT INTO [selenium].[ATPIP_PASSOS_INCLUSAO_PROPOSTA]
                                ([ID_INCLUSAO],
                                [ID_TIPO_PASSO],
                                [HRA_INICIO])
                                VALUES
                                (@idInclusao,
                                 1,                 
                                GETDATE()) SELECT SCOPE_IDENTITY()";

                command = new SqlCommand(query, connection);

                string _machineName = Environment.MachineName;

                command.Parameters.Add(new SqlParameter("@idInclusao", idTeste));               

                int primaryKey = Convert.ToInt32(command.ExecuteScalar());
                command.Dispose();
                return primaryKey;

            }
            catch (Exception e)
            {
                return 0;
            }
            finally
            {
                try
                {
                    connection.Close();
                }
                catch (Exception)
                {

                }
            }
        }

        public bool FechaResultadoPassoBase(
            int idPasso,
            string descriptionError,
            string descriptionEvidence,
            string statusPass,
            int idTipoPasso)
        {
            try
            {

                connection = conn.ReturnConnection(stringConnectResult);

                command = new SqlCommand();

                string query = @"UPDATE [selenium].[ATPIP_PASSOS_INCLUSAO_PROPOSTA] SET
                                    [HRA_FIM] = GETDATE(),
                                    [STA_PASSO] = @statusPasso,
                                    [ID_TIPO_PASSO] = @idTipoPasso,
                                    [DSC_ERRO] = @descricaoErro,
                                    [DSC_EVIDENCIA] = @descricaoEvidencia WHERE [ID_PASSOS] = @idPasso";


                command = new SqlCommand(query, connection);

                string _machineName = Environment.MachineName;

                command.Parameters.Add(new SqlParameter("@statusPasso", statusPass));
                command.Parameters.Add(new SqlParameter("@descricaoErro", descriptionError));
                command.Parameters.Add(new SqlParameter("@descricaoEvidencia", descriptionEvidence));
                command.Parameters.Add(new SqlParameter("@idPasso", idPasso));
                command.Parameters.Add(new SqlParameter("@idTipoPasso", idTipoPasso));


                int res = command.ExecuteNonQuery();
                command.Dispose();

                return res > 0;

            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                try
                {
                    connection.Close();
                }
                catch (Exception)
                {

                }
            }
        }

        public bool ExecuteQueryTabela(
            string orgao,
            string opereracao,
            string representante,
            string margem,
            string cpfCliente,
            string dataNasc,
            string matricula)
        {

            try
            {
                ReadFiles readFiles = new ReadFiles();

                connection = conn.ReturnConnection(stringConnectExecution);
                //cpfCliente = "05494591204";
                command = new SqlCommand();

                String query = readFiles.ReadFile(@"\tests\files\querys\inclusion\" + orgao + ".sql");
                query = readFiles.ReplaceQuery(query, representante, margem.Replace(",", "."), cpfCliente, dataNasc, matricula);

                command = new SqlCommand(query, connection);
                int ret = command.ExecuteNonQuery();
                command.Dispose();

                return ret > 0;

            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                try
                {
                    connection.Close();
                }
                catch (Exception)
                {

                }
            }
        }

        public List<string> BuscaCPF(string idCenario)
        {

            try
            {

                connection = conn.ReturnConnection(stringConnectResult);
                List<string> list = new List<string>();

                command = new SqlCommand();

                string query = @"SELECT 
                                [NUM_CPF], 
                                [VLR_MARGEM], 
                                [COD_MATRICULA], 
                                [DTA_NASCIMENTO] 
                                FROM [selenium].[ATCXC_CENARIO_X_CPF] 
                                WHERE ID_CENARIO = @idCenario ORDER BY NEWID();";


                command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@idCenario", idCenario);


                SqlDataReader reader = command.ExecuteReader();
                {
                    while (reader.Read())
                    {
                        list.Add(reader["NUM_CPF"] + ";" + reader["VLR_MARGEM"] + ";" + reader["COD_MATRICULA"] + ";" + reader["DTA_NASCIMENTO"]);
                    }
                    return list;
                }
            }
            catch (Exception e)
            {
                //carrega a mensagem da validação
                SetUp.GetInstance().MessageError.Add("*** ERRO AO BUSCAR O CPF ***: " + e);
                return null;
            }
            finally
            {
                try
                {
                    connection.Close();
                }
                catch (Exception)
                {

                }
            }

        }

    }
}
