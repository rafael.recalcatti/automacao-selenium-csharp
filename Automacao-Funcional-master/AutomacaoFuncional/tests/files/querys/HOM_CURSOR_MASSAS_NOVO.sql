--use DFOLHAesp

declare @num_upag_sec int
declare @cod_financeira int
declare @id_cenario INT
declare @sta_executar bit
declare @cod_ORGAO int
declare @ORGAO varchar(200)
declare @idadeMin int
declare @idadeMax int
declare @cod_operacao varchar(15) --1 - Margem Livre, 2 - Refin, 3 - Compra de d�vida
DECLARE @COD_SABEMI INT 
declare @cod_desconto int


--Cursor para percorrer os cen�rios 
DECLARE cursor_massas_novos CURSOR FOR

select ID_CENARIO,NUM_UPAG_SEC,COD_OPERACAO,COD_FINANCEIRA,COD_SABEMI 
from AUTOMACAOhom.selenium.ATCHI_CENARIOS_HABILITADOS_INCLUSAO CHI
where CHI.STA_EXECUTAR = 1 AND CHI.STA_CLIENTE_NOVO = 1


OPEN cursor_massas_novos

FETCH NEXT FROM cursor_massas_novos INTO @ID_CENARIO,@NUM_UPAG_SEC,@COD_OPERACAO,@COD_FINANCEIRA,@COD_SABEMI 

--Percorrendo linhas do cursor (enquanto houverem)
WHILE @@FETCH_STATUS = 0
BEGIN


SET @cod_ORGAO = (select COD_ORGAO from BASEUNICAesp.dbo.BUC_ORGAO bo where bo.NUM_UPAG_PADRAO = @num_upag_sec)
 --SETA COD_ORGAO SE RETORNAR VAZIO
 IF @COD_ORGAO IS NULL SET @COD_ORGAO = 0000

SET @ORGAO = (SELECT 
CASE
WHEN (case tds.COD_DESCONTO when 1 then tds.NME_DESCONTO else org.NME_ORGAO end) LIKE '%AERONAUTICA%' THEN 'AERONAUTICA'
WHEN (case tds.COD_DESCONTO when 0 then tds.NME_DESCONTO else org.NME_ORGAO end) LIKE '%SIAPE%' THEN 'SIAPE'
WHEN (case tds.COD_DESCONTO when 2 then tds.NME_DESCONTO else org.NME_ORGAO end) LIKE '%INSS%' THEN 'INSS'
WHEN (case tds.COD_DESCONTO when 1 then tds.NME_DESCONTO else org.NME_ORGAO end) LIKE '%MARINHA%' THEN 'MARINHA'
WHEN (case tds.COD_DESCONTO when 1 then tds.NME_DESCONTO else org.NME_ORGAO end) LIKE '%EXERCITO%' THEN 'EXERCITO'
WHEN (case tds.COD_DESCONTO when 6 then tds.NME_DESCONTO else org.NME_ORGAO end) LIKE '%GOVERNO%' THEN 'GOV'
END
FROM AUTOMACAOhom.selenium.ATCHI_CENARIOS_HABILITADOS_INCLUSAO ch 
left join DFOLHAesp.dbo.AFUPS_UPAG_SEC ups
on ups.NUM_UPAG_SEC = ch.NUM_UPAG_SEC
left join DFOLHAesp.dbo.AFSOR_SUB_ORGAO sor
on sor.COD_SUBORGAO = ups.COD_SUBORGAO
left join DFOLHAesp.dbo.AFORG_ORGAO org
on org.NUM_ORGAO = sor.NUM_ORGAO
left join DFOLHAesp.dbo.AFTDS_TIPO_DESCONTO tds
on tds.COD_DESCONTO = org.COD_DESCONTO
left join DFOLHAesp.dbo.AFFIN_FINANCEIRA fin
on fin.COD_FINANCEIRA = ch.COD_FINANCEIRA
left join DFOLHAesp.dbo.AFREP_REPRESENTANTE REP
on rep.COD_SABEMI = ch.COD_SABEMI
left join DFOLHAesp.dbo.AFTRP_TPO_REPRESENTANTE trp
on trp.COD_TPO_REPRESENTANTE = rep.COD_TPO_REPRESENTANTE WHERE ch.ID_CENARIO = @id_cenario)

SET @cod_desconto = (SELECT org.COD_DESCONTO
FROM AUTOMACAOhom.selenium.ATCHI_CENARIOS_HABILITADOS_INCLUSAO ch 
left join DFOLHAesp.dbo.AFUPS_UPAG_SEC ups
on ups.NUM_UPAG_SEC = ch.NUM_UPAG_SEC
left join DFOLHAesp.dbo.AFSOR_SUB_ORGAO sor
on sor.COD_SUBORGAO = ups.COD_SUBORGAO
left join DFOLHAesp.dbo.AFORG_ORGAO org
on org.NUM_ORGAO = sor.NUM_ORGAO
left join DFOLHAesp.dbo.AFTDS_TIPO_DESCONTO tds
on tds.COD_DESCONTO = org.COD_DESCONTO
left join DFOLHAesp.dbo.AFFIN_FINANCEIRA fin
on fin.COD_FINANCEIRA = ch.COD_FINANCEIRA
left join DFOLHAesp.dbo.AFREP_REPRESENTANTE REP
on rep.COD_SABEMI = ch.COD_SABEMI
left join DFOLHAesp.dbo.AFTRP_TPO_REPRESENTANTE trp
on trp.COD_TPO_REPRESENTANTE = rep.COD_TPO_REPRESENTANTE WHERE ch.ID_CENARIO = @id_cenario)

SELECT TOP 1 @idadeMin = lia.NUM_IDADE_MINIMA, @idadeMax = lia.NUM_IDADE_MAXIMA FROM AFTRF_TABELA_REGRA_FINANCEIRA trf
inner join AFLIA_LIMITE_IDADE_AF lia
on lia.COD_REGRA_FINANCEIRA = trf.COD_REGRA_FINANCEIRA
WHERE 
trf.STA_ATIVA = 1
and trf.DESC_REGRA LIKE '%'+@ORGAO+'%' and trf.COD_FINANCEIRA = @cod_financeira

SELECT TOP 1 @idadeMin = lia.NUM_IDADE_MINIMA, @idadeMax = lia.NUM_IDADE_MAXIMA FROM AFTRF_TABELA_REGRA_FINANCEIRA trf
inner join AFLIA_LIMITE_IDADE_AF lia
on lia.COD_REGRA_FINANCEIRA = trf.COD_REGRA_FINANCEIRA
WHERE 
trf.STA_ATIVA = 1
and trf.DESC_REGRA LIKE '%'+@ORGAO+'%' and trf.COD_FINANCEIRA = @cod_financeira



SELECT distinct top 5 bpf.NUM_CPF 
into ##temp_massas_novo
FROM BASEUNICAesp.dbo.BUC_PESSOA_FISICA bpf
INNER JOIN BASEUNICAesp.dbo.BUC_DADOS_FUNCIONAIS bdf
      ON bpf.COD_PESSOA_FISICA = bdf.COD_PESSOA_FISICA
left JOIN DFOLHAesp.dbo.AFPFI_PESSOA_FISICA pfi
      ON SUBSTRING('0',1,len(bpf.NUM_CPF)-11)+bpf.NUM_CPF = pfi.CPF
WHERE 
       -- �rg�o
     bdf.COD_ORGAO = @cod_ORGAO
     -- LIMITANDO IDADE     
     AND bpf.DTA_NASC BETWEEN  DATEADD(year,-@idadeMax,getdate()) AND DATEADD(year,-@idadeMin,getdate())
     --    -- RETIRANDO CPFs INVALIDOS
     and (bpf.NUM_CPF is not null and bpf.NUM_CPF <> '' and len(bpf.NUM_CPF) = 11)
     and pfi.CPF is null
       and  bpf.NUM_CPF not in(select num_cpf from AUTOMACAOhom.selenium.ATCXC_CENARIO_X_CPF)

---fim select
  insert into AUTOMACAOhom.selenium.ATCXC_CENARIO_X_CPF
  select distinct @id_cenario,tm.NUM_CPF, bpf.DTA_NASC, case WHEN @cod_financeira = 68 AND @num_upag_sec = 3 THEN 30 WHEN @cod_financeira in (23,68,71) AND @num_upag_sec = 4 THEN 30 WHEN @cod_financeira = 68 AND @COD_DESCONTO = 0 THEN 29 ELSE 600 end AS 'VLR_MARGEM', 
   (select top 1 COD_MATRICULA from BASEUNICAesp.dbo.BUC_DADOS_FUNCIONAIS where COD_PESSOA_FISICA = bpf.cod_pessoa_fisica) as cod_matricula
  from ##temp_massas_novo tm
  inner join BASEUNICAesp.dbo.BUC_PESSOA_FISICA bpf
  on tm.NUM_CPF = bpf.NUM_CPF


  drop table ##temp_massas_novo
  --Lendo a pr�xima linha
  
  FETCH NEXT FROM cursor_massas_novos INTO @ID_CENARIO,@NUM_UPAG_SEC,@COD_OPERACAO,@COD_FINANCEIRA,@COD_SABEMI 

END


-- Fechando Cursor para leitura
CLOSE cursor_massas_novos
-- Desalocando o cursor
DEALLOCATE cursor_massas_novos 
