--use DFOLHAtest
--CURSOR MASSAS REFIN
declare @num_upag_sec int
declare @cod_financeira int
declare @tpo_representante tinyint --1 loja, 2 --Representante 
declare @cod_operacao varchar(15) --1 - Margem Livre, 2 - Refin, 3 - Compra de d�vida
declare @id_cenario INT
DECLARE @COD_SABEMI INT 
declare @sta_executar bit
declare @ORGAO varchar(200)
declare @idadeMin int
declare @idadeMax int


DELETE FROM AUTOMACAOesp.selenium.ATCXC_CENARIO_X_CPF WHERE AUTOMACAOesp.selenium.ATCXC_CENARIO_X_CPF.ID_CENARIO IN(
SELECT ATCHI.ID_CENARIO FROM AUTOMACAOesp.selenium.ATCHI_CENARIOS_HABILITADOS_INCLUSAO ATCHI WHERE ATCHI.COD_OPERACAO = '2')

--Cursor para percorrer os cen�rios 
DECLARE cursor_massas CURSOR FOR

SELECT ID_CENARIO,NUM_UPAG_SEC,COD_OPERACAO,COD_FINANCEIRA,COD_SABEMI 
FROM AUTOMACAOesp.selenium.ATCHI_CENARIOS_HABILITADOS_INCLUSAO CHI
WHERE CHI.STA_EXECUTAR = 1 AND CHI.STA_CLIENTE_NOVO = 0 AND CHI.COD_OPERACAO = '2'


OPEN cursor_massas

FETCH NEXT FROM cursor_massas INTO @ID_CENARIO,@NUM_UPAG_SEC,@COD_OPERACAO,@COD_FINANCEIRA,@COD_SABEMI

--Percorrendo linhas do cursor (enquanto houverem)
WHILE @@FETCH_STATUS = 0
BEGIN


select top 5 cpf 
into ##temp_massas from AFPFI_PESSOA_FISICA pfi
inner join AFPRF_PROPOSTA_FUNCIONARIO prf
on prf.COD_PESSOA_FISICA = pfi.COD_PESSOA_FISICA
inner join AFEPP_EMPRESTIMO_PROPOSTA epp
on epp.COD_PROPOSTA = prf.COD_PROPOSTA
inner join AFUPS_UPAG_SEC ups
on ups.NUM_UPAG_SEC = prf.NUM_UPAG_SEC
inner join AFSOR_SUB_ORGAO sor
on sor.COD_SUBORGAO = ups.COD_SUBORGAO
inner join AFORG_ORGAO org
on org.NUM_ORGAO = sor.NUM_ORGAO
left join (
  select count(*) as qtd,COD_PROPOSTA 
  from AFPRE_PARCELA_EMPRESTIMO pre
  where pre.STA_QUITACAO = 1
  group by COD_PROPOSTA
)pagas
on pagas.COD_PROPOSTA = epp.COD_PROPOSTA
where COD_SITUACAO_AF = 0
and COD_SUB_SITUACAO_AF = 44
and epp.STA_QUITACAO = 0
and pfi.DTA_SINISTRO is null
and pfi.STA_FALECIDO = 0
and pfi.ATESTADO_OBITO = 0
and pagas.qtd> 36
--Seleciona Desconto ||SIAPE / GOV (cod_desconto) || MARINHA/ AERONAUTICA / MEX / INSS (num_upag_sec)
and prf.NUM_UPAG_SEC = @num_upag_sec ORDER BY NEWID()
--and org.COD_DESCONTO= 0


---fim select



  INSERT INTO AUTOMACAOesp.selenium.ATCXC_CENARIO_X_CPF
  SELECT DISTINCT @id_cenario,tm.cpf, PFI.DTA_NASC, 550, 
   (SELECT TOP 1 COD_MATRICULA FROM AFFNC_FUNCIONARIO WHERE COD_PESSOA_FISICA = pfi.cod_pessoa_fisica) AS cod_matricula
  FROM ##temp_massas tm
  inner join AFPFI_PESSOA_FISICA pfi
  ON tm.cpf = pfi.CPF


   DROP TABLE  ##temp_massas
  --Lendo a pr�xima linha
  
  FETCH NEXT FROM cursor_massas INTO @ID_CENARIO,@NUM_UPAG_SEC,@COD_OPERACAO,@COD_FINANCEIRA,@COD_SABEMI 

END

  --UPDATE chi
  --SET STA_EXECUTAR = 0
  --FROM AUTOMACAOesp.selenium.ATCHI_CENARIOS_HABILITADOS_INCLUSAO chi
  --WHERE chi.STA_DESATIVAR = 0 AND COD_OPERACAO = '2'
  --and chi.ID_CENARIO not in(SELECT DISTINCT ID_CENARIO FROM AUTOMACAOesp.selenium.ATCXC_CENARIO_X_CPF WHERE COD_OPERACAO = '2')


-- Fechando Cursor para leitura
CLOSE cursor_massas
-- Desalocando o cursor
DEALLOCATE cursor_massas 