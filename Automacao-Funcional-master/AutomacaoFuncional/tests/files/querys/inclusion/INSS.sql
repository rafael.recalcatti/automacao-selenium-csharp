/*
  Inserindo consulta com sucesso para pré-análise INSS
  Obs.: deve ser informado o código do representante em cod_sabemi
  Data: 25/04/2013
*/

--*************************************!!! E ROLLBACK COMENTADOS!!!********************************************

--*******************************************************************************************************************

	DECLARE @CPF VARCHAR(11)
	DECLARE @COD_REPR VARCHAR(4)
	DECLARE @COD_SABEMI VARCHAR(4)
	DECLARE @DTA_NASC DATETIME
	DECLARE @COD_CONSULTA VARCHAR(11)
	DECLARE @COD_MATRICULA VARCHAR (50)
	DECLARE @COD_MATRICULA_COMPLEMENTAR VARCHAR (50)
	DECLARE @margem VARCHAR(7)
	
	declare @desconto int
	declare @num_upag_sec int
	declare @cod_financeira int  --12 financeira sabemi, 23 daycaval, 68 PAN, 71 BONSUCESSO
	declare @tpo_representante tinyint --1 loja, 2 --Representante 
	declare @cod_operacao int --1 - Margem Livre, 2 - Refin, 3 - Compra de dívida
	declare @idade_min int
	declare @idade_max int
	--***************************!!!INFORMAR AQUI O REPRESENTANTE A SER UTILIZADO!!!*************************************

	  SET @desconto = 2
      SET @num_upag_sec = 4
	  SET @COD_SABEMI = ':COD_SABEMI'
	  --SET @num_upag_sec = '792'
	  SET @margem = ':MARGEM'
	  SET @CPF = ':CPF'
	      
	--*******************************************************************************************************************
	
	SET @COD_REPR = (SELECT TOP 1 COD_REPR FROM AFREP_REPRESENTANTE WHERE COD_SABEMI = @COD_SABEMI)
	
	SET @DTA_NASC = (SELECT DTA_NASC FROM AFPFI_PESSOA_FISICA where CPF= @CPF)

	IF @DTA_NASC IS NULL SET @DTA_NASC = ':DATA_NASC'

	SET @COD_MATRICULA = (SELECT top 1 func.COD_MATRICULA FROM AFFNC_FUNCIONARIO func INNER JOIN AFPFI_PESSOA_FISICA pfi
								on func.COD_PESSOA_FISICA = pfi.COD_PESSOA_FISICA
								WHERE pfi.CPF = @CPF)

	IF @COD_MATRICULA IS NULL SET @COD_MATRICULA = ':MATRICULA'

	SET @COD_MATRICULA_COMPLEMENTAR = (SELECT top 1 func.COD_MATRICULA_COMPLEMENTAR FROM AFFNC_FUNCIONARIO func INNER JOIN AFPFI_PESSOA_FISICA pfi
								on func.COD_PESSOA_FISICA = pfi.COD_PESSOA_FISICA
								WHERE pfi.CPF = @CPF)	

    IF @COD_MATRICULA_COMPLEMENTAR IS NULL SET @COD_MATRICULA_COMPLEMENTAR = 'MATRICULA'											

	--INSERT NA CMG
	INSERT INTO dbo.AFCMG_CONSULTA_MARGEM(DTA_CONSULTA,
		COD_REPR,
		CPF,
		COD_MATRICULA,
		DTA_RETORNO,
		VLR_MARGEM,
		STA_CONSULTA,
		STA_ATIVO,
		NUM_ORGAO,
		COD_DESCONTO,
		DTA_NASC,
		STA_CONSULTA_NETCERTO,
		DSC_RETORNO)
	VALUES(GETDATE(),
		@COD_REPR,
		@CPF,
		@COD_MATRICULA,
		GETDATE(),
		@margem,
		3,
		1,
		4,
		2,
		@DTA_NASC,
		1,
		'INFORMAR ESPÉCIE BENEFÍCIO IGUAL A 42 OU 46.')
