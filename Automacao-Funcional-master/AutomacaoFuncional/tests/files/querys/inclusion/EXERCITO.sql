/*
  Inserindo consulta com sucesso para pré-análise exército utilizando o cpf de um cliente do dfolha.
  Obs.: deve ser informado o código do representante em cod_sabemi
  Data: 25/04/2013
*/

--*************************************!!! E ROLLBACK COMENTADOS!!!********************************************

--*******************************************************************************************************************

	DECLARE @CPF VARCHAR(11)
	DECLARE @COD_REPR VARCHAR(4)
	DECLARE @COD_SABEMI VARCHAR(4)
	DECLARE @DTA_NASC DATETIME
	DECLARE @COD_CONSULTA VARCHAR(11)
	DECLARE @COD_MATRICULA VARCHAR (50)
	DECLARE @COD_MATRICULA_COMPLEMENTAR VARCHAR (50)
		
	declare @desconto int
	declare @num_upag_sec int
	declare @cod_financeira int  --12 financeira sabemi, 23 daycaval, 68 PAN, 71 BONSUCESSO
	declare @tpo_representante tinyint --1 loja, 2 --Representante 
	declare @cod_operacao int --1 - Margem Livre, 2 - Refin, 3 - Compra de dívida
	declare @idade_min int
	declare @idade_max int
	declare @margem VARCHAR(7)


	--***************************!!!INFORMAR AQUI O REPRESENTANTE A SER UTILIZADO!!!*************************************
	  SET @desconto = 2
      SET @num_upag_sec = 1
	  SET @COD_SABEMI = ':COD_SABEMI'
	  --SET @num_upag_sec = '792'
	  SET @margem = ':MARGEM'
	  SET @CPF = ':CPF'   
	
	
	--*******************************************************************************************************************
	
	SET @COD_REPR = (SELECT TOP 1 COD_REPR FROM AFREP_REPRESENTANTE WHERE COD_SABEMI = @COD_SABEMI)

	SET @DTA_NASC = (SELECT TOP 1 AFPFI.DTA_NASC FROM AFPFI_PESSOA_FISICA AFPFI WHERE CPF = @CPF)

	IF @DTA_NASC IS NULL SET @DTA_NASC = ':DATA_NASC'

	SET @COD_MATRICULA = --'234567891'
		(SELECT TOP 1 AFPRF.COD_MATRICULA FROM AFPRF_PROPOSTA_FUNCIONARIO AFPRF
			INNER JOIN AFPFI_PESSOA_FISICA AFPFI
			ON AFPRF.COD_PESSOA_FISICA = AFPFI.COD_PESSOA_FISICA
			WHERE AFPFI.CPF = @CPF AND (AFPRF.NUM_UPAG_SEC = 1)) --OR AFPRF.NUM_UPAG_SEC = 3))
		
	IF @COD_MATRICULA IS NULL SET @COD_MATRICULA = ':MATRICULA'

	SET @COD_MATRICULA_COMPLEMENTAR = --'234567891'
		(SELECT TOP 1 AFPRF.COD_MATRICULA_COMPLEMENTAR FROM AFPRF_PROPOSTA_FUNCIONARIO AFPRF
			INNER JOIN AFPFI_PESSOA_FISICA AFPFI
			ON AFPRF.COD_PESSOA_FISICA = AFPFI.COD_PESSOA_FISICA
			WHERE AFPFI.CPF = @CPF AND (AFPRF.NUM_UPAG_SEC = 1)) --OR AFPRF.NUM_UPAG_SEC = 3))

	IF @COD_MATRICULA_COMPLEMENTAR IS NULL SET @COD_MATRICULA_COMPLEMENTAR = ':MATRICULA'		


	--INSERT NA CMG
	INSERT INTO dbo.AFCMG_CONSULTA_MARGEM
	(DTA_CONSULTA,
	COD_REPR,
	CPF,
	COD_MATRICULA,
	DSC_SENHA_CCHEQUE,
	DTA_RETORNO,
	VLR_MARGEM,
	STA_CONSULTA,
	DSC_RETORNO,
	STA_ATIVO,
	COD_CERTIFICADO,
	NUM_ORGAO,
	COD_DESCONTO,
	COD_MATRICULA_COMPLEMENTAR,
	DTA_NASC,
	COD_ORIGEM_MARGEM)
	VALUES(
	GETDATE(),
	@COD_REPR,
	@CPF,
	@COD_MATRICULA,
	'123456789',
	GETDATE(),
	@margem,
	2,
	'INSERT TESTE DANI',
	1,
	81,
	1,
	1,
	@COD_MATRICULA_COMPLEMENTAR,
	@DTA_NASC,
	1)

