use DFOLHAdsv

declare @num_upag_sec int
declare @cod_financeira int
declare @tpo_representante tinyint --1 loja, 2 --Representante 
declare @cod_operacao varchar(15) --1 - Margem Livre, 2 - Refin, 3 - Compra de d�vida
declare @id_cenario INT
DECLARE @COD_SABEMI INT 
declare @sta_executar bit

delete from TESTCOMPLET.selenium.CENARIO_X_CPF

-- Cursor para percorrer os cen�rios 
DECLARE cursor_massas CURSOR FOR

select ID_CENARIO,NUM_UPAG_SEC,COD_OPERACAO,COD_FINANCEIRA,COD_SABEMI 
from TESTCOMPLET.selenium.CENARIOS_HABILITADOS CHA
where CHA.STA_EXECUTAR = 1


OPEN cursor_massas

FETCH NEXT FROM cursor_massas INTO @ID_CENARIO,@NUM_UPAG_SEC,@COD_OPERACAO,@COD_FINANCEIRA,@COD_SABEMI 

-- Percorrendo linhas do cursor (enquanto houverem)
WHILE @@FETCH_STATUS = 0
BEGIN

select @tpo_representante = cod_tpo_representante from AFREP_REPRESENTANTE where cod_sabemi = @COD_SABEMI

CREATE TABLE dbo.#t_cods_oper (COD_TPO_OPERACAO_PROPOSTA INTEGER NULL)
exec ('INSERT INTO dbo.#t_cods_oper SELECT COD_TPO_OPERACAO_PROPOSTA AS COD_TPO_OPERACAO_PROPOSTA FROM AFTOR_TPO_OPERACAO_PROPOSTA WHERE COD_TPO_OPERACAO_PROPOSTA IN ('+@cod_operacao+')')


SELECT distinct top 5  AFPFI.CPF as NUM_CPF
into ##temp_massas
FROM AFUPS_UPAG_SEC AFUPS
INNER JOIN AFFNC_FUNCIONARIO AFFNC
      ON AFFNC.NUM_UPAG_SEC = AFUPS.NUM_UPAG_SEC
INNER JOIN AFPFI_PESSOA_FISICA AFPFI
      ON AFFNC.COD_PESSOA_FISICA = AFPFI.COD_PESSOA_FISICA
INNER JOIN AFEUS_EMPRESA_UPAG_SEC AFEUS
      ON AFEUS.NUM_UPAG_SEC = AFUPS.NUM_UPAG_SEC
INNER JOIN AFFHB_FINANCEIRA_HABILITADA AFFHB
      ON AFFHB.COD_CANAL = AFEUS.COD_CANAL
      AND AFFHB.COD_EMPRESA = AFEUS.COD_EMPRESA
INNER JOIN AFTFH_TAB_FIN_HABILITADA AFTFH
      ON AFTFH.COD_CANAL = AFFHB.COD_CANAL
      AND AFTFH.COD_EMPRESA = AFFHB.COD_EMPRESA
      AND AFTFH.COD_FINANCEIRA = AFFHB.COD_FINANCEIRA
INNER JOIN AFTBE_TABELA_EMPRESTIMO AFTBE
      ON AFTBE.COD_TABELA_EMPRESTIMO = AFTFH.COD_TABELA_EMPRESTIMO
      and AFTBE.COD_FINANCEIRA = affhb.COD_FINANCEIRA
left join( 
             select COD_TABELA_EMPRESTIMO
             from configVenda.AFCTA_CONFIGURACAO_TABELA_AF AFCTA
             inner join configVenda.AFCAR_CONFIGURACAO_AF_REPRESENTANTE AFCAR
             on AFCTA.COD_CONFIGURACAO_AF = AFCAR.COD_CONFIGURACAO_AF
             where AFCAR.STA_BLOQUEADO = 0
             and AFCAR.COD_TPO_REPRESENTANTE = @tpo_representante
)tipo_representante
      on tipo_representante.COD_TABELA_EMPRESTIMO =  AFTBE.COD_TABELA_EMPRESTIMO
left join(
             select COD_TABELA_EMPRESTIMO
             from configVenda.AFCTA_CONFIGURACAO_TABELA_AF AFCTA
             inner join configVenda.AFCOS_CONFIGURACAO_OPERACAO_STATUS AFCOS
             on AFCOS.COD_CONFIGURACAO_AF = AFCTA.COD_CONFIGURACAO_AF
             where (AFCOS.COD_PERMISSAO_OPERACAO is null or AFCOS.COD_PERMISSAO_OPERACAO = 1)
             and AFCOS.COD_TPO_OPERACAO_PROPOSTA in(SELECT COD_TPO_OPERACAO_PROPOSTA FROM dbo.#t_cods_oper)
)operacoes
      on operacoes.COD_TABELA_EMPRESTIMO =  AFTBE.COD_TABELA_EMPRESTIMO 
left join(
             select count(PRF.COD_PROPOSTA) QTD,PRF.COD_PESSOA_FISICA
             from AFPRF_PROPOSTA_FUNCIONARIO PRF
			 INNER JOIN AFEPP_EMPRESTIMO_PROPOSTA EPP
			 ON EPP.COD_PROPOSTA = PRF.COD_PROPOSTA
			 AND EPP.COD_EMPRESA = PRF.COD_EMPRESA
			 WHERE EPP.COD_SITUACAO_AF IN(2,3)
			 AND PRF.COD_PESSOA_FISICA = COD_PESSOA_FISICA
			 GROUP BY PRF.COD_PESSOA_FISICA
)propostas_andamento
      on propostas_andamento.COD_PESSOA_FISICA = AFPFI.COD_PESSOA_FISICA

WHERE 
     -- SEM RESTRI��O
     NOT EXISTS (SELECT * FROM AFCLN_CADASTRO_LISTA_NEGRA AFCLN WHERE AFPFI.COD_PESSOA_FISICA = AFCLN.COD_PESSOA_FISICA)
	 -- UPAG
     AND AFUPS.NUM_UPAG_SEC = @num_upag_sec
     -- LIMITANDO IDADE     
     AND AFPFI.DTA_NASC BETWEEN DATEADD(year,-70,getdate()) AND DATEADD(year,-45,getdate())
     -- RETIRANDO FALECIDOS
     AND AFPFI.ATESTADO_OBITO IS NULL AND AFPFI.DTA_SINISTRO IS NULL AND AFPFI.STA_FALECIDO = 0 AND AFPFI.STA_FALECIDO = 0
     -- RETIRANDO CPFs INVALIDOS
     and AFPFI.CPF is not null and AFPFI.CPF <> '' and len(AFPFI.CPF) = 11
     -- CANAIS PARA VENDA
     and AFEUS.STA_OPERACAO = 1 --Operando
     and AFEUS.TPO_RUBRICA = 1  --Empr�stimo
     -- FINANCEIRA HABILITADA
     and AFFHB.COD_FINANCEIRA = @cod_financeira 
     -- Habilita��o ativa
     and aftfh.STA_ATIVO = 1
     -- Tabela exibida no site
     and aftbe.STA_SITE = 1
     -- Tabelas habilitadas por tipo de representante
     and tipo_representante.COD_TABELA_EMPRESTIMO is not null
     -- Tabelas habilitadas por opera��o
     and operacoes.COD_TABELA_EMPRESTIMO is not null
     -- and AFPFI.COD_PESSOA_FISICA = 82957
	 and  AFPFI.CPF not in(select num_cpf from TESTCOMPLET.selenium.CENARIO_X_CPF)
	 -- Retirar CPFs que possuam propostas em andamento
	 AND propostas_andamento.QTD is null



  insert into TESTCOMPLET.selenium.CENARIO_X_CPF
  select distinct @id_cenario,tm.NUM_CPF, PFI.DTA_NASC, 100 AS 'VLR_MARGEM', 
   (select top 1 COD_MATRICULA from AFFNC_FUNCIONARIO where COD_PESSOA_FISICA = pfi.cod_pessoa_fisica) as cod_matricula
  from ##temp_massas tm
  inner join AFPFI_PESSOA_FISICA pfi
  on tm.NUM_CPF = pfi.CPF




  DROP TABLE dbo.#t_cods_oper
  drop table ##temp_massas
  --Lendo a pr�xima linha
  FETCH NEXT FROM cursor_massas INTO @ID_CENARIO,@NUM_UPAG_SEC,@COD_OPERACAO,@COD_FINANCEIRA,@COD_SABEMI 

END

-- Fechando Cursor para leitura
CLOSE cursor_massas
-- Desalocando o cursor
DEALLOCATE cursor_massas 

