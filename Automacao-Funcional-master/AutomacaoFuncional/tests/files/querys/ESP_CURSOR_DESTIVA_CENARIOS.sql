--CURSOR DESTAIVAR CENARIOS


declare @num_upag_sec int
declare @cod_financeira int
declare @tpo_representante tinyint --1 loja, 2 --Representante 
declare @cod_operacao varchar(15) --1 - Margem Livre, 2 - Refin, 3 - Compra de d�vida
declare @id_cenario INT
DECLARE @COD_SABEMI INT 
declare @sta_executar bit


-- Cursor para percorrer os cen�rios 
DECLARE cursor_cenarios CURSOR FOR

select ID_CENARIO,NUM_UPAG_SEC,COD_OPERACAO,COD_FINANCEIRA,COD_SABEMI 
from AUTOMACAOesp.selenium.ATCHI_CENARIOS_HABILITADOS_INCLUSAO CHA
where CHA.STA_DESATIVAR = 0 

update AUTOMACAOesp.selenium.ATCHI_CENARIOS_HABILITADOS_INCLUSAO
set sta_executar = 1

OPEN cursor_cenarios

FETCH NEXT FROM cursor_cenarios INTO @ID_CENARIO,@NUM_UPAG_SEC,@COD_OPERACAO,@COD_FINANCEIRA,@COD_SABEMI 

-- Percorrendo linhas do cursor (enquanto houverem)
WHILE @@FETCH_STATUS = 0
BEGIN

select @tpo_representante = cod_tpo_representante from AFREP_REPRESENTANTE where cod_sabemi = @COD_SABEMI

CREATE TABLE dbo.#t_cods_oper (COD_TPO_OPERACAO_PROPOSTA INTEGER NULL)
exec ('INSERT INTO dbo.#t_cods_oper SELECT COD_TPO_OPERACAO_PROPOSTA AS COD_TPO_OPERACAO_PROPOSTA FROM AFTOR_TPO_OPERACAO_PROPOSTA WHERE COD_TPO_OPERACAO_PROPOSTA IN ('+@cod_operacao+')')


select
       @sta_executar = CASE COUNT(tb.COD_TABELA_EMPRESTIMO) WHEN 0 THEN 0 ELSE 1 END  
from (
SELECT top 1 AFTBE.COD_TABELA_EMPRESTIMO
FROM AFUPS_UPAG_SEC AFUPS
      INNER JOIN AFSOR_SUB_ORGAO AFSOR
      ON AFSOR.COD_SUBORGAO = AFUPS.COD_SUBORGAO
      INNER JOIN AFORG_ORGAO AFORG
      ON AFORG.NUM_ORGAO = AFSOR.NUM_ORGAO
      INNER JOIN AFTDS_TIPO_DESCONTO AFTDS
      ON AFORG.COD_DESCONTO = AFTDS.COD_DESCONTO
         INNER JOIN AFEUS_EMPRESA_UPAG_SEC AFEUS
         ON AFEUS.NUM_UPAG_SEC = AFUPS.NUM_UPAG_SEC
         INNER JOIN AFFHB_FINANCEIRA_HABILITADA AFFHB
      ON AFFHB.COD_CANAL = AFEUS.COD_CANAL
         AND AFFHB.COD_EMPRESA = AFEUS.COD_EMPRESA
         INNER JOIN AFTFH_TAB_FIN_HABILITADA AFTFH
         ON AFTFH.COD_CANAL = AFFHB.COD_CANAL
         AND AFTFH.COD_EMPRESA = AFFHB.COD_EMPRESA
         AND AFTFH.COD_FINANCEIRA = AFFHB.COD_FINANCEIRA
         INNER JOIN AFTBE_TABELA_EMPRESTIMO AFTBE
         ON AFTBE.COD_TABELA_EMPRESTIMO = AFTFH.COD_TABELA_EMPRESTIMO
         and AFTBE.COD_FINANCEIRA = affhb.COD_FINANCEIRA
             
         left join( 
             select COD_TABELA_EMPRESTIMO
             from configVenda.AFCTA_CONFIGURACAO_TABELA_AF AFCTA
             inner join configVenda.AFCAR_CONFIGURACAO_AF_REPRESENTANTE AFCAR
             on AFCTA.COD_CONFIGURACAO_AF = AFCAR.COD_CONFIGURACAO_AF
             where AFCAR.STA_BLOQUEADO = 0
             and AFCAR.COD_TPO_REPRESENTANTE = @tpo_representante
         )tipo_representante
         on tipo_representante.COD_TABELA_EMPRESTIMO =  AFTBE.COD_TABELA_EMPRESTIMO
         left join(
              select COD_TABELA_EMPRESTIMO
              from configVenda.AFCTA_CONFIGURACAO_TABELA_AF AFCTA
             inner join configVenda.AFCOS_CONFIGURACAO_OPERACAO_STATUS AFCOS
             on AFCOS.COD_CONFIGURACAO_AF = AFCTA.COD_CONFIGURACAO_AF
             where (AFCOS.COD_PERMISSAO_OPERACAO is null or AFCOS.COD_PERMISSAO_OPERACAO = 1)
             and AFCOS.COD_TPO_OPERACAO_PROPOSTA in(SELECT COD_TPO_OPERACAO_PROPOSTA FROM dbo.#t_cods_oper)
         )operacoes
         on operacoes.COD_TABELA_EMPRESTIMO =  AFTBE.COD_TABELA_EMPRESTIMO 
WHERE 
     AFUPS.NUM_UPAG_SEC = @num_upag_sec
         -- canais para venda
         and AFEUS.STA_OPERACAO = 1 --Operando
         and AFEUS.TPO_RUBRICA = 1  --Empr�stimo
         --financeira
         and AFFHB.COD_FINANCEIRA = @cod_financeira 
         --Habilita��o ativa
         and aftfh.STA_ATIVO = 1
         --Tabela exibida no site
         and aftbe.STA_SITE = 1
         --Tabelas habilitadas por tipo de representante
         and tipo_representante.COD_TABELA_EMPRESTIMO is not null
         --Tabelas habilitadas por opera��o
         and operacoes.COD_TABELA_EMPRESTIMO is not null
         -- and AFPFI.COD_PESSOA_FISICA = 82957
         AND AFTBE.STA_SITE = 1
)tb

       update AUTOMACAOesp.selenium.ATCHI_CENARIOS_HABILITADOS_INCLUSAO
       set sta_executar = @sta_executar
       where id_cenario = @id_cenario 

  DROP TABLE dbo.#t_cods_oper
  --Lendo a pr�xima linha
  FETCH NEXT FROM cursor_cenarios INTO @ID_CENARIO,@NUM_UPAG_SEC,@COD_OPERACAO,@COD_FINANCEIRA,@COD_SABEMI 

END

-- Fechando Cursor para leitura
CLOSE cursor_cenarios
-- Desalocando o cursor
DEALLOCATE cursor_cenarios 


--select *
--from  TESTCOMPLET.selenium.cenarios_habilitados
--where STA_EXECUTAR =  0
