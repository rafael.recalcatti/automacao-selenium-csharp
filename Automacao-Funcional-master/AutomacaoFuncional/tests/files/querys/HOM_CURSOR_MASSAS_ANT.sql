--use DFOLHAtest
--CURSOR MASSASAUTOMACAOhom
declare @num_upag_sec int
declare @cod_financeira int
declare @tpo_representante tinyint --1 loja, 2 --Representante 
declare @cod_operacao varchar(15) --1 - Margem Livre, 2 - Refin, 3 - Compra de d�vida
declare @id_cenario INT
DECLARE @COD_SABEMI INT 
declare @sta_executar bit
declare @ORGAO varchar(200)
declare @idadeMin int
declare @idadeMax int
declare @cod_desconto int


DELETE FROM AUTOMACAOhom.selenium.ATCXC_CENARIO_X_CPF

--Cursor para percorrer os cen�rios 
DECLARE cursor_massas CURSOR FOR

SELECT ID_CENARIO,NUM_UPAG_SEC,COD_OPERACAO,COD_FINANCEIRA,COD_SABEMI 
FROM AUTOMACAOhom.selenium.ATCHI_CENARIOS_HABILITADOS_INCLUSAO CHI
WHERE CHI.STA_EXECUTAR = 1 AND CHI.STA_CLIENTE_NOVO = 0 AND CHI.COD_OPERACAO != '2'


OPEN cursor_massas

FETCH NEXT FROM cursor_massas INTO @ID_CENARIO,@NUM_UPAG_SEC,@COD_OPERACAO,@COD_FINANCEIRA,@COD_SABEMI 

--Percorrendo linhas do cursor (enquanto houverem)
WHILE @@FETCH_STATUS = 0
BEGIN


select @tpo_representante = cod_tpo_representante from AFREP_REPRESENTANTE where cod_sabemi = @COD_SABEMI

CREATE TABLE dbo.#t_cods_oper (COD_TPO_OPERACAO_PROPOSTA INTEGER NULL)
exec ('INSERT INTO dbo.#t_cods_oper SELECT COD_TPO_OPERACAO_PROPOSTA AS COD_TPO_OPERACAO_PROPOSTA FROM AFTOR_TPO_OPERACAO_PROPOSTA WHERE COD_TPO_OPERACAO_PROPOSTA IN ('+@cod_operacao+')')

SET @ORGAO = (SELECT 
CASE
WHEN (case tds.COD_DESCONTO when 1 then tds.NME_DESCONTO else org.NME_ORGAO end) LIKE '%AERONAUTICA%' THEN 'AERONAUTICA'
WHEN (case tds.COD_DESCONTO when 0 then tds.NME_DESCONTO else org.NME_ORGAO end) LIKE '%SIAPE%' THEN 'SIAPE'
WHEN (case tds.COD_DESCONTO when 2 then tds.NME_DESCONTO else org.NME_ORGAO end) LIKE '%INSS%' THEN 'INSS'
WHEN (case tds.COD_DESCONTO when 1 then tds.NME_DESCONTO else org.NME_ORGAO end) LIKE '%MARINHA%' THEN 'MARINHA'
WHEN (case tds.COD_DESCONTO when 1 then tds.NME_DESCONTO else org.NME_ORGAO end) LIKE '%EXERCITO%' THEN 'EXERCITO'
WHEN (case tds.COD_DESCONTO when 6 then tds.NME_DESCONTO else org.NME_ORGAO end) LIKE '%GOVERNO%' THEN 'GOV'
END
FROM AUTOMACAOhom.selenium.ATCHI_CENARIOS_HABILITADOS_INCLUSAO ch 
left join DFOLHAesp.dbo.AFUPS_UPAG_SEC ups
on ups.NUM_UPAG_SEC = ch.NUM_UPAG_SEC
left join DFOLHAesp.dbo.AFSOR_SUB_ORGAO sor
on sor.COD_SUBORGAO = ups.COD_SUBORGAO
left join DFOLHAesp.dbo.AFORG_ORGAO org
on org.NUM_ORGAO = sor.NUM_ORGAO
left join DFOLHAesp.dbo.AFTDS_TIPO_DESCONTO tds
on tds.COD_DESCONTO = org.COD_DESCONTO
left join DFOLHAesp.dbo.AFFIN_FINANCEIRA fin
on fin.COD_FINANCEIRA = ch.COD_FINANCEIRA
left join DFOLHAesp.dbo.AFREP_REPRESENTANTE REP
on rep.COD_SABEMI = ch.COD_SABEMI
left join DFOLHAesp.dbo.AFTRP_TPO_REPRESENTANTE trp
on trp.COD_TPO_REPRESENTANTE = rep.COD_TPO_REPRESENTANTE WHERE ch.ID_CENARIO = @id_cenario)


SET @cod_desconto = (SELECT org.COD_DESCONTO
FROM AUTOMACAOhom.selenium.ATCHI_CENARIOS_HABILITADOS_INCLUSAO ch 
left join DFOLHAesp.dbo.AFUPS_UPAG_SEC ups
on ups.NUM_UPAG_SEC = ch.NUM_UPAG_SEC
left join DFOLHAesp.dbo.AFSOR_SUB_ORGAO sor
on sor.COD_SUBORGAO = ups.COD_SUBORGAO
left join DFOLHAesp.dbo.AFORG_ORGAO org
on org.NUM_ORGAO = sor.NUM_ORGAO
left join DFOLHAesp.dbo.AFTDS_TIPO_DESCONTO tds
on tds.COD_DESCONTO = org.COD_DESCONTO
left join DFOLHAesp.dbo.AFFIN_FINANCEIRA fin
on fin.COD_FINANCEIRA = ch.COD_FINANCEIRA
left join DFOLHAesp.dbo.AFREP_REPRESENTANTE REP
on rep.COD_SABEMI = ch.COD_SABEMI
left join DFOLHAesp.dbo.AFTRP_TPO_REPRESENTANTE trp
on trp.COD_TPO_REPRESENTANTE = rep.COD_TPO_REPRESENTANTE WHERE ch.ID_CENARIO = @id_cenario)

SELECT TOP 1 @idadeMin = lia.NUM_IDADE_MINIMA, @idadeMax = lia.NUM_IDADE_MAXIMA FROM AFTRF_TABELA_REGRA_FINANCEIRA trf
inner join AFLIA_LIMITE_IDADE_AF lia
on lia.COD_REGRA_FINANCEIRA = trf.COD_REGRA_FINANCEIRA
WHERE 
trf.STA_ATIVA = 1
and trf.DESC_REGRA LIKE '%'+@ORGAO+'%' and trf.COD_FINANCEIRA = @cod_financeira



SELECT top 5  AFPFI.CPF as NUM_CPF
into ##temp_massas
FROM AFUPS_UPAG_SEC AFUPS
INNER JOIN AFFNC_FUNCIONARIO AFFNC
      ON AFFNC.NUM_UPAG_SEC = AFUPS.NUM_UPAG_SEC
INNER JOIN AFPFI_PESSOA_FISICA AFPFI
      ON AFFNC.COD_PESSOA_FISICA = AFPFI.COD_PESSOA_FISICA
INNER JOIN AFEUS_EMPRESA_UPAG_SEC AFEUS
      ON AFEUS.NUM_UPAG_SEC = AFUPS.NUM_UPAG_SEC
INNER JOIN AFFHB_FINANCEIRA_HABILITADA AFFHB
      ON AFFHB.COD_CANAL = AFEUS.COD_CANAL
      AND AFFHB.COD_EMPRESA = AFEUS.COD_EMPRESA
INNER JOIN AFTFH_TAB_FIN_HABILITADA AFTFH
      ON AFTFH.COD_CANAL = AFFHB.COD_CANAL
     AND AFTFH.COD_EMPRESA = AFFHB.COD_EMPRESA
      AND AFTFH.COD_FINANCEIRA = AFFHB.COD_FINANCEIRA
INNER JOIN AFTBE_TABELA_EMPRESTIMO AFTBE
      ON AFTBE.COD_TABELA_EMPRESTIMO = AFTFH.COD_TABELA_EMPRESTIMO
      and AFTBE.COD_FINANCEIRA = affhb.COD_FINANCEIRA
INNER JOIN AFSOR_SUB_ORGAO AFSOR
         ON AFSOR.COD_SUBORGAO = AFUPS.COD_SUBORGAO
INNER JOIN AFORG_ORGAO AFORG
         ON AFORG.NUM_ORGAO = AFSOR.NUM_ORGAO
INNER JOIN BASEUNICAesp.dbo.BUC_PESSOA_FISICA PF 
         ON PF.NUM_CPF = AFPFI.CPF
INNER JOIN BASEUNICAesp.dbo.BUC_DESCONTO DS
         ON DS.COD_PESSOA_FISICA = PF.COD_PESSOA_FISICA
INNER JOIN BASEUNICAesp.dbo.BUC_RUBRICA RUB 
         ON DS.COD_RUBRICA = RUB.COD_RUBRICA
LEFT OUTER JOIN BASEUNICAesp.dbo.BUC_DADOS_FUNCIONAIS DF 
         ON DS.COD_DADO_FUNCIONAL = DF.COD_DADO_FUNCIONAL
LEFT JOIN AFTEU_TABELA_EMPRESTIMO_UF AFTEU
         ON AFTEU.COD_TABELA_EMPRESTIMO = AFTBE.COD_TABELA_EMPRESTIMO
LEFT JOIN configVenda.AFCTP_CONFIGURACAO_TABELA_PRESTAMISTA AFCTP
         ON AFCTP.COD_TABELA_EMPRESTIMO = AFTBE.COD_TABELA_EMPRESTIMO
left join( 
             select COD_TABELA_EMPRESTIMO
             from configVenda.AFCTA_CONFIGURACAO_TABELA_AF AFCTA
             inner join configVenda.AFCAR_CONFIGURACAO_AF_REPRESENTANTE AFCAR
             on AFCTA.COD_CONFIGURACAO_AF = AFCAR.COD_CONFIGURACAO_AF
             where AFCAR.STA_BLOQUEADO = 0
             and AFCAR.COD_TPO_REPRESENTANTE = @tpo_representante
)tipo_representante
      on tipo_representante.COD_TABELA_EMPRESTIMO =  AFTBE.COD_TABELA_EMPRESTIMO
left join(
             select COD_TABELA_EMPRESTIMO
             from configVenda.AFCTA_CONFIGURACAO_TABELA_AF AFCTA
             inner join configVenda.AFCOS_CONFIGURACAO_OPERACAO_STATUS AFCOS
             on AFCOS.COD_CONFIGURACAO_AF = AFCTA.COD_CONFIGURACAO_AF
             where 
                              (
                              (AFCOS.COD_PERMISSAO_OPERACAO is null or AFCOS.COD_PERMISSAO_OPERACAO = 1)
                                  and AFCOS.COD_TPO_OPERACAO_PROPOSTA in(SELECT COD_TPO_OPERACAO_PROPOSTA FROM dbo.#t_cods_oper)
                              
                                  AND
                                  
                                  NOT EXISTS   (SELECT COD_TABELA_EMPRESTIMO FROM configVenda.AFCTA_CONFIGURACAO_TABELA_AF cta
                                                      inner join configVenda.AFCOS_CONFIGURACAO_OPERACAO_STATUS fcos
                                                      on fcos.COD_CONFIGURACAO_AF = cta.COD_CONFIGURACAO_AF
                                                      WHERE (AFCOS.COD_PERMISSAO_OPERACAO = 1)
                                                      and AFCOS.COD_TPO_OPERACAO_PROPOSTA not in(SELECT COD_TPO_OPERACAO_PROPOSTA FROM dbo.#t_cods_oper))  
                              
                              --(AFCOS.COD_PERMISSAO_OPERACAO is null or AFCOS.COD_TPO_OPERACAO_PROPOSTA = 0)
                              --and AFCOS.COD_TPO_OPERACAO_PROPOSTA IN (SELECT COD_TPO_OPERACAO_PROPOSTA 
                                               --                                                FROM dbo.#t_cods_oper 
                                               --                                                WHERE dbo.#t_cods_oper.COD_TPO_OPERACAO_PROPOSTA <> AFCOS.COD_TPO_OPERACAO_PROPOSTA)
                              )
                       
)operacoes
      on operacoes.COD_TABELA_EMPRESTIMO =  AFTBE.COD_TABELA_EMPRESTIMO 
left join(
             select count(PRF.COD_PROPOSTA) QTD,PRF.COD_PESSOA_FISICA
             from AFPRF_PROPOSTA_FUNCIONARIO PRF
                    INNER JOIN AFEPP_EMPRESTIMO_PROPOSTA EPP
                    ON EPP.COD_PROPOSTA = PRF.COD_PROPOSTA
                    AND EPP.COD_EMPRESA = PRF.COD_EMPRESA
                    WHERE EPP.COD_SITUACAO_AF IN(2,3)
                    AND PRF.COD_PESSOA_FISICA = COD_PESSOA_FISICA
                    GROUP BY PRF.COD_PESSOA_FISICA
)propostas_andamento
      on propostas_andamento.COD_PESSOA_FISICA = AFPFI.COD_PESSOA_FISICA

WHERE 
     -- SEM RESTRI��O
     NOT EXISTS (SELECT * FROM AFCLN_CADASTRO_LISTA_NEGRA AFCLN WHERE AFPFI.COD_PESSOA_FISICA = AFCLN.COD_PESSOA_FISICA)
       -- UPAG
     AND AFUPS.NUM_UPAG_SEC = @num_upag_sec
     -- LIMITANDO IDADE     
     AND AFPFI.DTA_NASC BETWEEN  DATEADD(year,-@idadeMax,getdate()) AND DATEADD(year,-@idadeMin,getdate())
     -- RETIRANDO FALECIDOS
     AND AFPFI.ATESTADO_OBITO = 0 AND AFPFI.DTA_SINISTRO IS NULL AND AFPFI.STA_FALECIDO = 0 AND AFPFI.STA_FALECIDO = 0
     -- RETIRANDO CPFs INVALIDOS
     and AFPFI.CPF is not null and AFPFI.CPF <> '' and len(AFPFI.CPF) = 11
     -- CANAIS PARA VENDA
     and AFEUS.STA_OPERACAO = 1 --Operando
     and AFEUS.TPO_RUBRICA = 1  --Empr�stimo
     -- FINANCEIRA HABILITADA
     and AFFHB.COD_FINANCEIRA = @cod_financeira 
     -- Habilita��o ativa
     and aftfh.STA_ATIVO = 1
     -- Tabela exibida no site
     and aftbe.STA_SITE = 1
     -- Tabelas habilitadas por tipo de representante
     and tipo_representante.COD_TABELA_EMPRESTIMO is not null
     -- Tabelas habilitadas por opera��o
     and operacoes.COD_TABELA_EMPRESTIMO is not null
     -- and AFPFI.COD_PESSOA_FISICA = 82957
     and  AFPFI.CPF not in(select num_cpf from AUTOMACAOhom.selenium.ATCXC_CENARIO_X_CPF)
       -- Retirar CPFs que possuam propostas em andamento
     AND propostas_andamento.QTD is null
              -- Valida Contratos na Base �nica
       and 
              (   ( 
             (@num_upag_sec=1 or AFORG.COD_DESCONTO = 0)  
          

             AND (PF.NUM_CPF = AFPFI.CPF) 
             AND (DS.STA_ATIVO = 1) AND (DS.DTA_PRAZO_DESCONTO IS NOT NULL) 
             AND   (DATEDIFF(M, GETDATE(), DS.DTA_PRAZO_DESCONTO) > 0) 
             AND   (RUB.NME_RUBRICA NOT IN ('ZXY', 'ZXZ')) 
             AND   (DF.COD_MATRICULA = AFFNC.COD_MATRICULA OR DF.COD_MATRICULA IS NULL)
             ) 
             OR 
             
             (@num_upag_sec<>1 AND AFORG.COD_DESCONTO <> 0) 
             )

             -- Remove Tabelas de UFs Espec�ficas
       AND AFTEU.COD_TABELA_EMPRESTIMO IS NULL
       -- Remove Tabelas com Idade Espec�fica
       AND AFCTP.NUM_IDADE_MINIMA = 0 AND AFCTP.NUM_IDADE_MAXIMA = 100 ORDER BY NEWID()



---fim select



  insert into AUTOMACAOhom.selenium.ATCXC_CENARIO_X_CPF
  select distinct @id_cenario,tm.NUM_CPF, PFI.DTA_NASC, case WHEN @cod_financeira = 68 AND @num_upag_sec = 3 THEN 30 WHEN @cod_financeira in (23,68,71) AND @num_upag_sec = 4 THEN 30  WHEN @cod_financeira = 68 AND @COD_DESCONTO = 0 THEN 29 ELSE 600 end AS 'VLR_MARGEM', 
   (select top 1 COD_MATRICULA from AFFNC_FUNCIONARIO where COD_PESSOA_FISICA = pfi.cod_pessoa_fisica) as cod_matricula
  from ##temp_massas tm
  inner join AFPFI_PESSOA_FISICA pfi
  on tm.NUM_CPF = pfi.CPF


   DROP TABLE dbo.#t_cods_oper
   DROP TABLE ##temp_massas
  --Lendo a pr�xima linha
  
  FETCH NEXT FROM cursor_massas INTO @ID_CENARIO,@NUM_UPAG_SEC,@COD_OPERACAO,@COD_FINANCEIRA,@COD_SABEMI 

END

  --UPDATE chi
  --SET STA_EXECUTAR = 0
  --FROM AUTOMACAOhom.selenium.ATCHI_CENARIOS_HABILITADOS_INCLUSAO chi
  --WHERE chi.STA_DESATIVAR = 0 and COD_OPERACAO != '2'
  --and chi.ID_CENARIO not in(SELECT DISTINCT ID_CENARIO FROM AUTOMACAOhom.selenium.ATCXC_CENARIO_X_CPF ) 


-- Fechando Cursor para leitura
CLOSE cursor_massas
-- Desalocando o cursor
DEALLOCATE cursor_massas 

