﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using System.IO;
using System.Threading;
using System.Drawing;
using OpenQA.Selenium.Support.Extensions;
using System.Text.RegularExpressions;
using OpenQA.Selenium.Support.UI;

namespace InclusaoProposta.tests.utils
{
    class Util
    {
        private static Util util;

        private Util()
        {

        }

        public static Util GetInstance()
        {
            if (util == null)
            {
                util = new Util();
            }

            return util;
        }

        public void WaitElementPresent(IWebDriver driver, string _locator, int timeout)
        {
            try
            {

                By locator = By.XPath(_locator);
                Thread.Sleep(600);
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible((By)locator));
            }
            catch (Exception e)
            {
            }
        }

        public void WaitElementPresentList(IWebDriver driver, By locator, int timeout)
        {
            try
            {

                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.PresenceOfAllElementsLocatedBy(locator));
            }
            catch (Exception e)
            {
            }
        }

        public void WaitElementNotPresent(int timeout)
        {
            try
            {
                Thread.Sleep(400);
                By locator = By.XPath("//div[@id='Aguarde']");
                WebDriverWait wait = new WebDriverWait(SetUp.GetInstance().Driver, TimeSpan.FromSeconds(timeout));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(locator));

            }
            catch (Exception e)
            {
            }
        }

        public bool WaitForElementVisible(IWebElement element, int timeoutSecond)
        {
            int count = 0;

            do
            {
                try
                {
                    return element.Displayed && element.Enabled;
                }
                catch (Exception)
                {
                    Thread.Sleep(250);
                    count++;
                }

            } while (count < timeoutSecond * 4);

            return false;
        }

        public void RefreshPage(IWebDriver driver)
        {
            try
            {
                driver.Navigate().Refresh();
            }
            catch (Exception e)
            {

            }
        }

        public void HigthLine(IWebElement locator, bool arg)
        {
            try
            {
                string _color = arg ? "outline: 4px solid #00FF00;" : "outline: 4px solid #ff0000;";
                this.ScrollElementoPage(SetUp.GetInstance().Driver, locator, 10);
                IJavaScriptExecutor javaScriptExecutor = (IJavaScriptExecutor)SetUp.GetInstance().Driver;
                javaScriptExecutor.ExecuteScript("arguments[0].setAttribute('style', arguments[1]);",
                locator, _color);

            }
            catch (Exception) { }

        }

        public void ClickJS(IWebElement element)
        {
            try
            {
                this.HigthLine(element, true);
                IJavaScriptExecutor executor = (IJavaScriptExecutor)SetUp.GetInstance().Driver;
                executor.ExecuteScript("arguments[0].click();", element);

            }
            catch (Exception) { }

        }

        public void SendKeyJS(IWebElement element, string arg)
        {
            try
            {
                this.HigthLine(element, true);
                IJavaScriptExecutor executor = (IJavaScriptExecutor)SetUp.GetInstance().Driver;
                executor.ExecuteScript("arguments[0].value = '" + arg + "';", element);

            }
            catch (Exception) { }

        }

        public void ScrollElementoPage(IWebDriver driver, IWebElement element, int space)
        {
            try
            {

                Point point = new Point();

                if (element != null)
                {
                    point = element.Location;
                    IJavaScriptExecutor js = driver as IJavaScriptExecutor;

                    js.ExecuteScript("arguments[0].scrollIntoView(true);", element);
                    //js.ExecuteScript("window.scrollBy(0," + (point.Y - space) + ");");

                }
            }
            catch (Exception e)
            {
                // TODO: handle exception
            }
        }

        public void AcceptAlertpage()
        {

            try
            {

                SetUp.GetInstance().Driver.SwitchTo().Alert().Accept();

            }
            catch (Exception)
            {

            }

        }

        public void SendKeysInput(IWebElement element, string arg)
        {
            try
            {
                if ((element.GetAttribute("value") == null || element.GetAttribute("value") == "") && element.Displayed && element.Enabled)
                {
                    this.SendKeyJS(element, arg);
                }
            }
            catch (Exception) { }

        }

        public void SendKeysInputTextOnly(IWebElement element, string arg)
        {
            try
            {

                var a = !Regex.IsMatch(element.GetAttribute("value"), "^[a-zA-Z ]+$");
                if (a && element.Displayed && element.Enabled)
                {
                    this.HigthLine(element, true);
                    element.Clear();
                    element.SendKeys(arg);
                }
            }
            catch (Exception) { }

        }

        internal void SendKeysIputPostalCode(IWebElement inputPostalCodeCompany, IWebElement imgSearchPostalCode, IWebElement inputSearchPostalCode, IWebElement inputButtonSearchPostalCode, IWebElement selectPostalCode, string v)
        {
            try
            {
                imgSearchPostalCode.Click();
                Thread.Sleep(200);
                this.SendKeyJS(inputSearchPostalCode, v);
                Thread.Sleep(200);
                inputButtonSearchPostalCode.Click();
                this.WaitElementNotPresent(10);
                //Thread.Sleep(1000);
                selectPostalCode.Click();
                this.WaitElementNotPresent(10);
                //Thread.Sleep(800);
            }
            catch (Exception ex)
            {

            }



        }

        internal bool IsSelectedBank(IWebElement element)
        {
            try
            {
                return element.GetAttribute("value") == "Digite aqui para pesquisar" || element.GetAttribute("value") == "";
            }
            catch (Exception)
            {
                return false;
            }
        }

        internal bool IsValidCodePostal(IWebElement element)
        {
            try
            {
                return element.Displayed && element.Enabled;
            }
            catch (Exception)
            {
                return false;
            }
        }

        internal bool IsExistingElement(IWebElement element)
        {
            try
            {
                return element.Displayed && element.Enabled;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void SendKeysInputMaskCPF(IWebElement element, string arg)
        {
            try
            {

                if (element.Displayed && element.Enabled)
                {
                    this.SendKeyJS(element, arg);
                }
            }
            catch (Exception) { }

        }

        public void SendKeysInputMask(IWebElement element, string arg)
        {
            try
            {

                if ((element.GetAttribute("value") == null || element.GetAttribute("value") == "") && element.Displayed && element.Enabled)
                {
                    this.SendKeyJS(element, arg);
                }
            }
            catch (Exception) { }

        }

        public void SetSelect(IWebElement element, string arg)
        {
            try
            {
                SelectElement selectElement = new SelectElement(element);
                if (element.Displayed && element.Enabled)
                {
                    //if ((element.GetAttribute("value") == "-1" || element.Text.Contains("---") || selectElement.SelectedOption.Text.Contains("Selecione")))
                    // {
                    this.HigthLine(element, true);
                    selectElement.SelectByText(arg);
                    // }
                }

            }
            catch (Exception) { }

        }

        public void AcceptAlewrtpage()
        {

            try
            {

                SetUp.GetInstance().Driver.SwitchTo().Alert().Accept();

            }
            catch (Exception ex)
            {

            }

        }

        public string Screenshot(string strLog)
        {
            try
            {
                this.AcceptAlewrtpage();
                string dt = DateTime.Now.ToString("H_mm_dd_MM_y - ");
                string filePath = System.IO.Directory.GetParent(System.IO.Directory.GetParent(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)).FullName).FullName + @"\tests\files\reports\images\" + dt + strLog + ".PNG";
                Image img = GetEntireScreenshot();
                img.Save(filePath);            
                //SetUp.GetInstance().Driver.TakeScreenshot().SaveAsFile(filePath);
                return filePath;
            }
            catch (Exception)
            {
                return null;
            }


        }

        public Image GetEntireScreenshot()
        {
            // Get the total size of the page
            ((IJavaScriptExecutor)SetUp.GetInstance().Driver).ExecuteScript(String.Format("document.body.style.zoom = '75 %'"));
            ((IJavaScriptExecutor)SetUp.GetInstance().Driver).ExecuteScript(String.Format("window.scrollBy(0,-2000)"));
            var totalWidth = (int)(long)((IJavaScriptExecutor)SetUp.GetInstance().Driver).ExecuteScript("return document.body.offsetWidth"); //documentElement.scrollWidth");
            var totalHeight = (int)(long)((IJavaScriptExecutor)SetUp.GetInstance().Driver).ExecuteScript("return  document.body.parentNode.scrollHeight");
            // Get the size of the viewport
            var viewportWidth = (int)(long)((IJavaScriptExecutor)SetUp.GetInstance().Driver).ExecuteScript("return document.body.clientWidth"); //documentElement.scrollWidth");
            var viewportHeight = (int)(long)((IJavaScriptExecutor)SetUp.GetInstance().Driver).ExecuteScript("return window.innerHeight"); //documentElement.scrollWidth");

            // We only care about taking multiple images together if it doesn't already fit
            if (totalWidth <= viewportWidth && totalHeight <= viewportHeight)
            {
                var screenshot = SetUp.GetInstance().Driver.TakeScreenshot();
                return ScreenshotToImage(screenshot);
            }
            // Split the screen in multiple Rectangles
            var rectangles = new List<Rectangle>();
            // Loop until the totalHeight is reached
            for (var y = 0; y < totalHeight; y += viewportHeight)
            {
                var newHeight = viewportHeight;
                // Fix if the height of the element is too big
                if (y + viewportHeight > totalHeight)
                {
                    newHeight = totalHeight - y;
                }
                // Loop until the totalWidth is reached
                for (var x = 0; x < totalWidth; x += viewportWidth)
                {
                    var newWidth = viewportWidth;
                    // Fix if the Width of the Element is too big
                    if (x + viewportWidth > totalWidth)
                    {
                        newWidth = totalWidth - x;
                    }
                    // Create and add the Rectangle
                    var currRect = new Rectangle(x, y, newWidth, newHeight);
                    rectangles.Add(currRect);
                }
            }
            // Build the Image
            var stitchedImage = new Bitmap(totalWidth, totalHeight);
            // Get all Screenshots and stitch them together
            var previous = Rectangle.Empty;
            foreach (var rectangle in rectangles)
            {
                // Calculate the scrolling (if needed)
                if (previous != Rectangle.Empty)
                {
                    var xDiff = rectangle.Right - previous.Right;
                    var yDiff = rectangle.Bottom - previous.Bottom;
                    // Scroll
                    ((IJavaScriptExecutor)SetUp.GetInstance().Driver).ExecuteScript(String.Format("window.scrollBy({0}, {1})", xDiff, yDiff));
                }
                // Take Screenshot
                var screenshot = SetUp.GetInstance().Driver.TakeScreenshot();
                // Build an Image out of the Screenshot
                var screenshotImage = ScreenshotToImage(screenshot);
                // Calculate the source Rectangle
                var sourceRectangle = new Rectangle(viewportWidth - rectangle.Width, viewportHeight - rectangle.Height, rectangle.Width, rectangle.Height);
                // Copy the Image
                using (var graphics = Graphics.FromImage(stitchedImage))
                {
                    graphics.DrawImage(screenshotImage, rectangle, sourceRectangle, GraphicsUnit.Pixel);
                }
                // Set the Previous Rectangle
                previous = rectangle;
            }
            return stitchedImage;
        }

        private static Image ScreenshotToImage(Screenshot screenshot)
        {
            Image screenshotImage;
            using (var memStream = new MemoryStream(screenshot.AsByteArray))
            {
                screenshotImage = Image.FromStream(memStream);
            }

            return screenshotImage;
        }

        public void HoverMenu(string arg)
        {
            try
            {
                By locator = By.XPath("//td[@class='menuHorizontal']//td/a[text()='" + arg + "']");
                IWebElement element = SetUp.GetInstance().Driver.FindElement(locator);
                string mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
                IJavaScriptExecutor js = SetUp.GetInstance().Driver as IJavaScriptExecutor;
                js.ExecuteScript(mouseOverScript, element);

            }
            catch (Exception e)
            {

            }
        }

        public Int32 GetAge(string dateOfBirth)
        {
            int year = Int32.Parse(dateOfBirth.Split('/')[2]);
            int month = Int32.Parse(dateOfBirth.Split('/')[1]);
            int day = Int32.Parse(dateOfBirth.Split('/')[0]);
            DateTime dat = new DateTime(year, month, day);
            return new DateTime(DateTime.Now.Subtract(dat).Ticks).Year - 1;
        }

        /*
        public void waitElementClicable(WebDriver driver, By locator, int timeout)
        {
            try
            {
                Thread.sleep(600);
                WebDriverWait wait = new WebDriverWait(driver, timeout);
                wait.until(ExpectedConditions.elementToBeClickable(locator));
            }
            catch (Exception e)
            {
            }
        }

        public void waitElementSelect(WebDriver driver, By locator, int timeout)
        {
            try
            {
                Thread.sleep(600);
                WebDriverWait wait = new WebDriverWait(driver, timeout);
                wait.until(ExpectedConditions.elementToBeSelected(locator));
            }
            catch (Exception e)
            {
            }
        }
              
        public void scrollInicioPage(WebDriver driver)
        {
            try
            {
                JavascriptExecutor js = ((JavascriptExecutor)driver);
                js.executeScript("window.scrollBy(0,-1500);");
                Thread.sleep(1000);
            }
            catch (Exception e)
            {
            }
        }

        public void scrollListElementoPage(WebDriver driver, List<WebElement> element, int espaco)
        {
            try
            {
                Point ponto = null;
                if (element != null)
                {
                    for (WebElement webElement : element)
                    {
                        ponto = webElement.getLocation();
                        break;
                    }

                    JavascriptExecutor js = ((JavascriptExecutor)driver);
                    js.executeScript("window.scrollTo(0," + (ponto.y - espaco) + ");");
                    Thread.sleep(2000);
                }
            }
            catch (Exception e)
            {
            }
        }
        */

    }

    public static class Log
    {
        public static void Write(string msg)
        {
            Console.WriteLine(msg);
        }

        public static void WriteFormat(string msgFormat, params object[] args)
        {
            string formattedString = string.Format(msgFormat, args);
            Console.WriteLine(formattedString);
        }

        public static void WriteLineFormat(string msgFormat, params object[] args)
        {
            string formattedString = string.Format(msgFormat, args);
            Console.WriteLine(formattedString);
            Console.WriteLine();
        }

        public static void WriteStringLine()
        {
            Console.WriteLine(new string('-', 30));
        }

        public static void WriteLine()
        {
            Console.WriteLine();
        }
    }

    /*
            public static void logPrint(bool _status, String strLog)
            {

                String strLogFormatado = formatarNomeLog(strLog);
                ExtentTest extentTest = TestRule.getExtentTest();
                String caminhoImagen = System.getProperty("user.dir") + "\\src\\test\\resources\\reports\\inclusao\\"
                        + (strLogFormatado) + ".png";

                TestRule.caminhoUltIgmage = caminhoImagen;
                DadosPDF dadosPDF = new DadosPDF();

                dadosPDF.setName(caminhoImagen);
                dadosPDF.setSurname(strLog);
                dadosPDF.setStatus(bool);

                // adiciona os dados na lista para criação do PDF
                TestRule.itensPDF.add(dadosPDF);

                try
                {
                    efetuarPrintTela(strLogFormatado);
                    if (bool)
                    {
                        extentTest.log(Status.PASS, strLog,
                                MediaEntityBuilder.createScreenCaptureFromPath(caminhoImagen).build());

                        // MediaEntityBuilder
                        // .createScreenCaptureFromPath(System.getProperty("user.dir")
                        // + "\\src\\test\\resources\\reports\\inclusao\\" + (strLogFormatado) + ".png")
                        // .build());
                    }
                    else {
                        extentTest.log(Status.FAIL, strLog,
                                MediaEntityBuilder.createScreenCaptureFromPath(caminhoImagen).build());
                        // MediaEntityBuilder
                        // .createScreenCaptureFromPath(System.getProperty("user.dir")
                        // + "\\src\\test\\resources\\reports\\inclusao\\" + (strLogFormatado) + ".png")
                        // .build());
                    }
                }
                catch (IOException e)
                {

                    e.printStackTrace();
                }
            }

        */

}

