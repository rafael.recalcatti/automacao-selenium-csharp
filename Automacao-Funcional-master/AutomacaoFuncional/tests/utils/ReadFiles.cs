﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InclusaoProposta.tests.utils
{
    class ReadFiles
    {

        public String ReadFile(string file_arg)
        {
            try
            {
                String text;
                string filePath = System.IO.Directory.GetParent(System.IO.Directory.GetParent(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)).FullName).FullName + file_arg;
                var fileStream = new FileStream(filePath , FileMode.Open, FileAccess.Read);

                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))

                {
                    text = streamReader.ReadToEnd();
                }

                Console.WriteLine(text);
                return text;
            }
            catch (Exception e)
            { 
                return null;
            }
        }

        public String ReplaceQuery(string query, string repre, string marge, string cpf, string dataNasc, string matricula)
        {
            try
            {
                query = query.Replace(":COD_SABEMI", repre).Replace(":MARGEM", marge).Replace(":CPF", cpf).Replace(":DATA_NASC", dataNasc).Replace(":MATRICULA", matricula);
           

                return query;
            }
            catch (Exception e)
            {
                return "";
            }
        }
    }
}
