﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;
using InclusaoProposta.tests.db;
using System.Collections.Generic;

namespace InclusaoProposta.tests.utils

{

    public class SetUp
    {

        private IWebDriver driver;
        private static SetUp setUp;
        private string cpf;
        private string dataNascimento;
        private int idade;
        private double margem;
        private static List<string> listaCpf;

        private string cadastro;
        private string operacao;
        private int idTeste;
        private string user;
        private string password;
        private string oldMsgResultadoPasso;
        private static List<string> messageError;
        private int posicaoListaCPF;
        private int tamanhoListaCPF;
        private bool scenarioInfo;

        //static List<string> listaParametros;
        //static List<string> listCenariosIgnorados;

        private string codigoCenario;
        private int codigoTipoPasso;
        private int codigoPasso;
        private string nomeCenario;
        private string url;        
        private string ambienteExecucao;
        private string baseExecucao;
        private string orgao;
        private string financeira;
        private string tipoCliente;
        private string tipoOperacao;
        private string tipoLoja;


        private string codRepresentante;
        private string matricula;
        private string codigoProposta;      
        private string codigoPropostaAprovada;
        private string codigoTabela;
        private string resultadoProposta;


        public IWebDriver Driver
        {
            get
            {
                return driver;
            }

            set
            {
                driver = value;
            }
        }

        private SetUp()
        {

        }

        public static SetUp GetInstance()
        {
            if (setUp == null)
            {                
                setUp = new SetUp();
                messageError = new List<string>();
                listaCpf = new List<string>();
            }

            return setUp;
        }

        public void KillInstance()
        {
            driver = null;
            setUp = null;
        }

        public void SetUpDriver()
        {

            try
            {
                string url = this.AmbienteExecucao.Equals("Espelho") ? "http://representanteonlineesp.sabemi.com.br/Default.aspx" :
                              this.AmbienteExecucao.Equals("Desenvolvimento") ? "http://carina/SABEMIDSV/PORTAL/RepresentanteOnline/Default.aspx" :
                              this.AmbienteExecucao.Equals("Desenvolvimento 1") ? "http://carina/SABEMIDSV1/PORTAL/RepresentanteOnline/Default.aspx" :
                              this.AmbienteExecucao.Equals("Desenvolvimento 2") ? "http://carina/SABEMIDSV2/PORTAL/RepresentanteOnline/Default.aspx" :
                              this.AmbienteExecucao.Equals("Desenvolvimento 3") ? "http://carina/SABEMIDSV3/PORTAL/RepresentanteOnline/Default.aspx" :
                              this.AmbienteExecucao.Equals("Desenvolvimento 4") ? "http://carina/SABEMIDSV4/PORTAL/RepresentanteOnline/Default.aspx" :
                              this.AmbienteExecucao.Equals("Desenvolvimento 5") ? "http://carina/SABEMIDSV5/PORTAL/RepresentanteOnline/Default.aspx" :
                              this.AmbienteExecucao.Equals("Desenvolvimento 6") ? "http://carina/SABEMIDSV6/PORTAL/RepresentanteOnline/Default.aspx" :
                              this.AmbienteExecucao.Equals("Desenvolvimento 7") ? "http://carina/SABEMIDSV7/PORTAL/RepresentanteOnline/Default.aspx" :
                              this.AmbienteExecucao.Equals("Desenvolvimento 8") ? "http://carina/SABEMIDSV8/PORTAL/RepresentanteOnline/Default.aspx" :
                              this.AmbienteExecucao.Equals("Desenvolvimento 9") ? "http://carina/SABEMIDSV9/PORTAL/RepresentanteOnline/Default.aspx" :
                              this.AmbienteExecucao.Equals("Homologação") ? "http://carina/SABEMIHOM/PORTAL/RepresentanteOnline/Default.aspx" :
                              this.AmbienteExecucao.Equals("Teste 1") ? "http://carina/SABEMITEST1/PORTAL/RepresentanteOnline/Default.aspx" :
                              this.AmbienteExecucao.Equals("Teste 2") ? "http://carina/SABEMITEST2/PORTAL/RepresentanteOnline/Default.aspx" :
                              this.AmbienteExecucao.Equals("Teste 3") ? "http://carina/SABEMITEST3/PORTAL/RepresentanteOnline/Default.aspx" :
                              this.AmbienteExecucao.Equals("Teste 4") ? "http://carina/SABEMITEST4/PORTAL/RepresentanteOnline/Default.aspx" :
                                                                                 "http://carina/SABEMITEST/PORTAL/RepresentanteOnline/Default.aspx";
                var optionsChrome = new ChromeOptions();
                Driver = new ChromeDriver(optionsChrome);
                Driver.Manage().Window.Maximize();
                Driver.Navigate().GoToUrl(url);

            }
            catch (Exception e)
            {

            }


        }

        public void QuitDriver()
        {

            try
            {
                driver.Quit();
                KillInstance();
            }
            catch (Exception)
            {

            }

        }

        public string LogResultado()
        {
            string _return = null;

            try
            {
                _return = " URL[" + this.Driver.Url + "]";
            }
            catch (Exception ex)
            {
                _return = " URL NAO ACESSIVEL[" + ex + "]";
            }

            _return += " USUARIO[" + User + "]";
            _return += " SENHA[" + Password + "]";
            _return += " CPF[" + Cpf + "]";
            _return += " MATRICULA[" + Matricula + "]";
            _return += " IDADE[" + Margem + "]";
            _return += " COD_REP[" + CodRepresentante + "]";
            _return += " COD_PROPOSTA[" + CodigoProposta + "]";
            _return += " ID_TESTE[" + IdTeste + "]";
            _return += " AMBIENTE_EXE[" + AmbienteExecucao + "]";
            _return += " BASE_EXE[" + BaseExecucao + "]";
            _return += " -->>LOG";

            foreach (string Message in this.MessageError)
            {

                _return += "[" + Message + "]";

            }

            return _return;
        }

        public List<string> MessageError
        {
            get
            {
                return messageError;
            }

            set
            {
                messageError.Add(value.ToString());
            }
        }

        public string Cpf
        {
            get
            {
                return cpf;
            }

            set
            {
                cpf = value;
            }
        }

        public int IdTeste
        {
            get
            {
                return idTeste;
            }

            set
            {
                idTeste = value;
            }
        }

        public string User
        {
            get
            {
                return user;
            }

            set
            {
                user = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public int PosicaoListaCPF
        {
            get
            {
                return posicaoListaCPF;
            }

            set
            {
                posicaoListaCPF = value;
            }
        }

        public string AmbienteExecucao
        {
            get
            {
                return ambienteExecucao;
            }

            set
            {
                ambienteExecucao = value;
            }
        }

        public string BaseExecucao
        {
            get
            {
                return baseExecucao;
            }

            set
            {
                baseExecucao = value;
            }
        }

        public string CodRepresentante
        {
            get
            {
                return codRepresentante;
            }

            set
            {
                codRepresentante = value;
            }
        }

        public string CodigoProposta
        {
            get
            {
                return codigoProposta;
            }

            set
            {
                codigoProposta = value;
            }
        }

        public string CodigoPropostaAprovada
        {
            get
            {
                return codigoPropostaAprovada;
            }

            set
            {
                codigoPropostaAprovada = value;
            }
        }

        public string Matricula
        {
            get
            {
                return matricula;
            }

            set
            {
                matricula = value;
            }
        }

        public string NomeCenario
        {
            get
            {
                return nomeCenario;
            }

            set
            {
                nomeCenario = value;
            }
        }

        public string CodigoCenario
        {
            get
            {
                return codigoCenario;
            }

            set
            {
                codigoCenario = value;
            }
        }

        public string Orgao
        {
            get
            {
                return orgao;
            }

            set
            {
                orgao = value;
            }
        }

        public string Financeira
        {
            get
            {
                return financeira;
            }

            set
            {
                financeira = value;
            }
        }

        public string TipoCliente
        {
            get
            {
                return tipoCliente;
            }

            set
            {
                tipoCliente = value;
            }
        }

        public string TipoOperacao
        {
            get
            {
                return tipoOperacao;
            }

            set
            {
                tipoOperacao = value;
            }
        }

        public string TipoLoja
        {
            get
            {
                return tipoLoja;
            }

            set
            {
                tipoLoja = value;
            }
        }

        public string ResultadoProposta
        {
            get
            {
                return resultadoProposta;
            }

            set
            {
                resultadoProposta = value;
            }
        }

        public int CodigoTipoPasso
        {
            get
            {
                return codigoTipoPasso;
            }

            set
            {
                codigoTipoPasso = value;
            }
        }

        public int CodigoPasso
        {
            get
            {
                return codigoPasso;
            }

            set
            {
                codigoPasso = value;
            }
        }

        public string CodigoTabela
        {
            get
            {
                return codigoTabela;
            }

            set
            {
                codigoTabela = value;
            }
        }

        public int TamanhoListaCPF
        {
            get
            {
                return tamanhoListaCPF;
            }

            set
            {
                tamanhoListaCPF = value;
            }
        }

        public int Idade
        {
            get
            {
                return idade;
            }

            set
            {
                idade = value;
            }
        }

        public double Margem
        {
            get
            {
                return margem;
            }

            set
            {
                margem = value;
            }
        }

        public string DataNascimento
        {
            get
            {
                return dataNascimento;
            }

            set
            {
                dataNascimento = value;
            }
        }

        public List<string> ListaCpf
        {
            get
            {
                return listaCpf;
            }

            set
            {
                listaCpf = value;
            }
        }
    }

}
