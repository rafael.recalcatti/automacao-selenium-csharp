﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net.Mime;
using System.Xml;
using System.Globalization;

namespace EmailApp
{
    public class MailClass
    {

        static public void Send(string arg)
        {

            var a = Environment.GetEnvironmentVariable("Ambiente de Execução").Equals("Espelho") ? "PROD" : "DEV";
            Console.WriteLine(">>>>SENDING EMAIL FOR:" + a + " GROUP<<<<<");
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            DateTime localDate = DateTime.Now;
            client.Host = "mailsvrprod01.sabemi.com.br";
            client.EnableSsl = true;
            client.Credentials = new System.Net.NetworkCredential("psi@sabemi.com.br", "");
            MailMessage mail = new MailMessage();
            mail.Sender = new System.Net.Mail.MailAddress("psi@sabemi.com.br", "SABEMI");
            mail.From = new MailAddress("psi@sabemi.com.br");
            //a = "DEV";
            mail.To.Add(("resultado.automacao." + a + "@sabemi.com.br"));
            mail.Subject = "[SABEMI] Resultado da Automação Inclusão de Proposta ["+Environment.GetEnvironmentVariable("Ambiente de Execução").ToUpper() + "]";

            string emailPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            emailPath = System.IO.Directory.GetParent(emailPath).FullName;
            emailPath = System.IO.Directory.GetParent(emailPath).FullName;
            Console.WriteLine(">>>>EMAIL BODY PATH:::"+ emailPath + "<<<<<<");
            var contentID = "companylogo";
            var logo = new Attachment(emailPath + @"\logo_sabemi.png");
            string strMail;

            logo.ContentId = contentID;
            logo.ContentDisposition.Inline = true;
            logo.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
            List<string> _status = ReadFile(arg);
                   
            var times = GetTimes(arg.Replace("html", "xml"));

            using (StreamReader objReader = new StreamReader(emailPath + @"\template_email.html"))
            {
                // Lê todo o arquivo e o joga em uma variável
                strMail = objReader.ReadToEnd();
            }


            strMail = strMail.Replace("{startTime}", times[0]);
            strMail = strMail.Replace("{endTime}", times[1]);
            strMail = strMail.Replace("{totalTime}", times[2]);
            strMail = strMail.Replace("{passou}", _status[0]);
            strMail = strMail.Replace("{falhou}", _status[1]);
            strMail = strMail.Replace("{ignorado}", _status[2]);
            strMail = strMail.Replace("{inconclusivo}", _status[3]);
            strMail = strMail.Replace("{total}", _status[4]);


            mail.Attachments.Add(logo);
            mail.Attachments.Add(new Attachment(arg));
            mail.Body = strMail;
            mail.IsBodyHtml = true;           
            
            mail.Priority = MailPriority.High;
            try
            {
                
                client.Send(mail);
                Console.WriteLine(">>>>EMAIL SENT<<<<<");
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(">>>>EXCEPTION EMAIL:::" + ex + "<<<<<<<<<<");
            }
            finally
            {
                mail = null;
            }

        }
       
        public static List<string> ReadFile(string filePath)
        {
            try
            {
                String text;
                List<string> list = new List<string>();                              
                var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))

                {
                    text = streamReader.ReadToEnd();
                }
               
                list.Add(text.Substring(text.IndexOf("hidden total-passou'>"),36).Split('>')[1].Split('<')[0]);
                list.Add(text.Substring(text.IndexOf("hidden total-falhou'>"), 36).Split('>')[1].Split('<')[0]);
                list.Add(text.Substring(text.IndexOf("hidden total-ignorado'>"), 36).Split('>')[1].Split('<')[0]);
                list.Add(text.Substring(text.IndexOf("hidden total-massa'>"), 36).Split('>')[1].Split('<')[0]);
                list.Add(text.Substring(text.IndexOf("hidden total-tests'>"), 36).Split('>')[1].Split('<')[0]);
              
                return list;
            }
            catch (Exception)
            {
                return null;
            }
        }

        static private List<string> GetTimes(string pathXML)
        {
            try
            {

                XmlTextReader xmlFile = new XmlTextReader(pathXML);

                List<string> list = new List<string>();

                while (xmlFile.Read())
                {
                    if (xmlFile.NodeType == XmlNodeType.Element && xmlFile.Name == "test-run")
                    {

                        string dateString = xmlFile.GetAttribute("start-time");
                       
                        DateTime dtStart = DateTime.Parse(dateString,
                                                  System.Globalization.CultureInfo.InvariantCulture);

                        dateString = xmlFile.GetAttribute("end-time");

                        DateTime dtEnd = DateTime.Parse(dateString,
                                                  System.Globalization.CultureInfo.InvariantCulture);

                        string totalTime = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                             (int)dtEnd.Subtract(dtStart).Hours,
                                                  dtEnd.Subtract(dtStart).Minutes,
                                                  dtEnd.Subtract(dtStart).Seconds,
                                                  dtEnd.Subtract(dtStart).Milliseconds);


                        list.Add(dtStart.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture));
                        list.Add(dtEnd.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture));
                        list.Add(totalTime);
                        Console.WriteLine(">>>>LIST TIMES:" + list[0] + "<<<<<");


                        break;
                    }

                }

                //xmlFile.Close();

                //XmlDocument xmlDoc = new XmlDocument();

                //xmlDoc.Load(pathXML);

                //XmlNode node = xmlDoc.SelectSingleNode("test-run");
                //node.Attributes[13].Value = "teste";

                //xmlDoc.Save(pathXML);

                return list;


            }
            catch (Exception ex)
            {
                Console.WriteLine(">>>>ERROR XML READ:" + ex + "<<<<<");
                return null;
            }
        }


    }
}
