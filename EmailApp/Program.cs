﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace EmailApp
{
    class Program : MailClass
    {
        static void Main(string[] args)
        {

            MailClass.Send(RunCommand());

        }

        static public string RunCommand()
        {
            try
            {

                List<string> list = new List<string>();

                string pathPackages = System.IO.Directory.GetParent(System.IO.Directory.GetParent(System.IO.Directory.GetParent(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)).FullName).FullName).FullName + @"\packages\ReportUnit.1.2.1\tools\ReportUnitSabemi.exe";

                string xmlPath = System.IO.Directory.GetParent(System.IO.Directory.GetParent(System.IO.Directory.GetParent(System.IO.Directory.GetParent(System.IO.Directory.GetParent(System.IO.Directory.GetParent(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)).FullName).FullName).FullName).FullName).FullName).FullName + @"\TestResult.xml";

                string pathDebug = System.IO.Directory.GetParent(System.IO.Directory.GetParent(System.IO.Directory.GetParent(System.IO.Directory.GetParent(System.IO.Directory.GetParent(System.IO.Directory.GetParent(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)).FullName).FullName).FullName).FullName).FullName).FullName + @"\TestResult.html";

                string a = '"' + pathPackages + '"' + "  " + '"' + xmlPath + '"' + "" + "  " + '"' + pathDebug + '"';

                Console.WriteLine(">>>COMMAND:::" + a);

                Process proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = a,
                        //Arguments = a,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };

                proc.Start();

                while (!proc.StandardOutput.EndOfStream)
                {
                    string s = proc.StandardOutput.ReadLine();
                    // do something with line
                }

                Console.WriteLine(">>>COMMAND RETURN:::" + pathDebug);
                return pathDebug;
              
            }
            catch (Exception ex)
            {               
                Console.WriteLine(">>>ERRO:::" + ex);
                return null;
            }
        }

    }
}
